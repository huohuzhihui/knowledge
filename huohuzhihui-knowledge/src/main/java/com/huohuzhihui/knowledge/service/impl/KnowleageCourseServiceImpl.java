package com.huohuzhihui.knowledge.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.knowledge.mapper.KnowleageCourseMapper;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.service.IKnowleageCourseService;

/**
 * 知识/课程Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@Service
public class KnowleageCourseServiceImpl implements IKnowleageCourseService 
{
    @Autowired
    private KnowleageCourseMapper knowleageCourseMapper;

    /**
     * 查询知识/课程
     * 
     * @param courseId 知识/课程ID
     * @return 知识/课程
     */
    @Override
    public KnowleageCourse selectKnowleageCourseById(Long courseId)
    {
        return knowleageCourseMapper.selectKnowleageCourseById(courseId);
    }

    /**
     * 查询知识/课程列表
     * 
     * @param knowleageCourse 知识/课程
     * @return 知识/课程
     */
    @Override
    public List<KnowleageCourse> selectKnowleageCourseList(KnowleageCourse knowleageCourse)
    {
        return knowleageCourseMapper.selectKnowleageCourseList(knowleageCourse);
    }

    /**
     * 新增知识/课程
     * 
     * @param knowleageCourse 知识/课程
     * @return 结果
     */
    @Override
    public int insertKnowleageCourse(KnowleageCourse knowleageCourse)
    {
        knowleageCourse.setCreateTime(DateUtils.getNowDate());
        return knowleageCourseMapper.insertKnowleageCourse(knowleageCourse);
    }

    /**
     * 修改知识/课程
     * 
     * @param knowleageCourse 知识/课程
     * @return 结果
     */
    @Override
    public int updateKnowleageCourse(KnowleageCourse knowleageCourse)
    {
        knowleageCourse.setUpdateTime(DateUtils.getNowDate());
        return knowleageCourseMapper.updateKnowleageCourse(knowleageCourse);
    }

    /**
     * 批量删除知识/课程
     * 
     * @param courseIds 需要删除的知识/课程ID
     * @return 结果
     */
    @Override
    public int deleteKnowleageCourseByIds(Long[] courseIds)
    {
        return knowleageCourseMapper.deleteKnowleageCourseByIds(courseIds);
    }

    /**
     * 删除知识/课程信息
     * 
     * @param courseId 知识/课程ID
     * @return 结果
     */
    @Override
    public int deleteKnowleageCourseById(Long courseId)
    {
        return knowleageCourseMapper.deleteKnowleageCourseById(courseId);
    }
}
