package com.huohuzhihui.knowledge.service;

import java.util.List;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;

/**
 * 知识分类Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public interface IKnowledgeCategoryService 
{
    /**
     * 查询知识分类
     * 
     * @param categoryId 知识分类ID
     * @return 知识分类
     */
    public KnowledgeCategory selectKnowledgeCategoryById(Long categoryId);

    /**
     * 查询知识分类列表
     * 
     * @param knowledgeCategory 知识分类
     * @return 知识分类集合
     */
    public List<KnowledgeCategory> selectKnowledgeCategoryList(KnowledgeCategory knowledgeCategory);

    /**
     * 新增知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    public int insertKnowledgeCategory(KnowledgeCategory knowledgeCategory);

    /**
     * 修改知识分类
     * 
     * @param knowledgeCategory 知识分类
     * @return 结果
     */
    public int updateKnowledgeCategory(KnowledgeCategory knowledgeCategory);

    /**
     * 批量删除知识分类
     * 
     * @param categoryIds 需要删除的知识分类ID
     * @return 结果
     */
    public int deleteKnowledgeCategoryByIds(Long[] categoryIds);

    /**
     * 删除知识分类信息
     * 
     * @param categoryId 知识分类ID
     * @return 结果
     */
    public int deleteKnowledgeCategoryById(Long categoryId);
}
