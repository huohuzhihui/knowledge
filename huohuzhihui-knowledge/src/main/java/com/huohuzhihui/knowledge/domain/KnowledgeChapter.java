package com.huohuzhihui.knowledge.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 课程章节对象 knowledge_chapter
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public class KnowledgeChapter extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long chapterId;

    /** 章节标题 */
    @Excel(name = "章节标题")
    private String chapterTitle;

    /** 所属课程 */
    @Excel(name = "所属课程")
    private Long courseId;

    /** 内容类型：0图文1音频2视频 */
    @Excel(name = "内容类型：0图文1音频2视频")
    private Integer contentType;

    /** 章节介绍 */
    @Excel(name = "章节介绍")
    private String chapterDescription;

    /** 章节内容存储地址 */
    @Excel(name = "章节内容存储地址")
    private String chapterPath;

    /** 章节顺序号 */
    @Excel(name = "章节顺序号")
    private Long chapterSort;

    /** 免费试看/试听时长 */
    private Long chapterFreeTimes;



    public void setChapterId(Long chapterId) 
    {
        this.chapterId = chapterId;
    }

    public Long getChapterId() 
    {
        return chapterId;
    }
    public void setChapterTitle(String chapterTitle) 
    {
        this.chapterTitle = chapterTitle;
    }

    public String getChapterTitle() 
    {
        return chapterTitle;
    }
    public void setCourseId(Long courseId) 
    {
        this.courseId = courseId;
    }

    public Long getCourseId() 
    {
        return courseId;
    }
    public void setContentType(Integer contentType) 
    {
        this.contentType = contentType;
    }

    public Integer getContentType() 
    {
        return contentType;
    }
    public void setChapterDescription(String chapterDescription) 
    {
        this.chapterDescription = chapterDescription;
    }

    public String getChapterDescription() 
    {
        return chapterDescription;
    }
    public void setChapterPath(String chapterPath) 
    {
        this.chapterPath = chapterPath;
    }

    public String getChapterPath() 
    {
        return chapterPath;
    }
    public void setChapterSort(Long chapterSort) 
    {
        this.chapterSort = chapterSort;
    }

    public Long getChapterSort() 
    {
        return chapterSort;
    }

    public Long getChapterFreeTimes() {
        return chapterFreeTimes;
    }

    public void setChapterFreeTimes(Long chapterFreeTimes) {
        this.chapterFreeTimes = chapterFreeTimes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("chapterId", getChapterId())
            .append("chapterTitle", getChapterTitle())
            .append("courseId", getCourseId())
            .append("contentType", getContentType())
            .append("chapterDescription", getChapterDescription())
            .append("chapterPath", getChapterPath())
            .append("chapterSort", getChapterSort())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
