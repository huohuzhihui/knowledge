package com.huohuzhihui.knowledge.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 知识/课程对象 knowleage_course
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
public class KnowleageCourse extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long courseId;

    /** 课程分类 */
    @Excel(name = "课程分类")
    private Long categoryId;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 章节数 */
    @Excel(name = "章节数")
    private Long chaptersNum;

    /** 描述 */
    @Excel(name = "描述")
    private String courseDescription;

    /** 封面图 */
    @Excel(name = "封面图")
    private String courseImgs;

    /** 原价 */
    @Excel(name = "原价")
    private BigDecimal courseOldPrice;

    /** 现价 */
    @Excel(name = "现价")
    private BigDecimal coursePrice;

    /** 是否热门：0不是1是 */
    @Excel(name = "是否热门：0不是1是")
    private Integer courseHot;

    /** 是否最新：0不是1是 */
    @Excel(name = "是否最新：0不是1是")
    private Integer courseNew;

    /** 销量 */
    @Excel(name = "销量")
    private Long courseSales;

    /** 是否支持试看:0不支持1支持 */
    @Excel(name = "是否支持试看:0不支持1支持")
    private Integer courseTry;

    /** 试看页数 */
    @Excel(name = "试看页数")
    private Long courseTryPages;

    /** 试看时长，单位为秒 */
    @Excel(name = "试看时长，单位为秒")
    private Long courseTryTimes;

    /** 审核状态：0待审核1审核通过2审核不通过 */
    @Excel(name = "审核状态：0待审核1审核通过2审核不通过")
    private Integer auditStatus;

    /** 上架状态：0未上架1已上架 */
    @Excel(name = "上架状态：0未上架1已上架")
    private Integer sellStatus;

    /** 媒体类型：0图文1音频2视频3直播*/
    private Integer courseMedia;

    public void setCourseId(Long courseId) 
    {
        this.courseId = courseId;
    }

    public Long getCourseId() 
    {
        return courseId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }
    public void setCourseName(String courseName) 
    {
        this.courseName = courseName;
    }

    public String getCourseName() 
    {
        return courseName;
    }
    public void setChaptersNum(Long chaptersNum) 
    {
        this.chaptersNum = chaptersNum;
    }

    public Long getChaptersNum() 
    {
        return chaptersNum;
    }
    public void setCourseDescription(String courseDescription) 
    {
        this.courseDescription = courseDescription;
    }

    public String getCourseDescription() 
    {
        return courseDescription;
    }
    public void setCourseImgs(String courseImgs) 
    {
        this.courseImgs = courseImgs;
    }

    public String getCourseImgs() 
    {
        return courseImgs;
    }
    public void setCourseOldPrice(BigDecimal courseOldPrice) 
    {
        this.courseOldPrice = courseOldPrice;
    }

    public BigDecimal getCourseOldPrice() 
    {
        return courseOldPrice;
    }
    public void setCoursePrice(BigDecimal coursePrice) 
    {
        this.coursePrice = coursePrice;
    }

    public BigDecimal getCoursePrice() 
    {
        return coursePrice;
    }
    public void setCourseHot(Integer courseHot) 
    {
        this.courseHot = courseHot;
    }

    public Integer getCourseHot() 
    {
        return courseHot;
    }
    public void setCourseNew(Integer courseNew) 
    {
        this.courseNew = courseNew;
    }

    public Integer getCourseNew() 
    {
        return courseNew;
    }
    public void setCourseSales(Long courseSales) 
    {
        this.courseSales = courseSales;
    }

    public Long getCourseSales() 
    {
        return courseSales;
    }
    public void setCourseTry(Integer courseTry) 
    {
        this.courseTry = courseTry;
    }

    public Integer getCourseTry() 
    {
        return courseTry;
    }
    public void setCourseTryPages(Long courseTryPages) 
    {
        this.courseTryPages = courseTryPages;
    }

    public Long getCourseTryPages() 
    {
        return courseTryPages;
    }
    public void setCourseTryTimes(Long courseTryTimes) 
    {
        this.courseTryTimes = courseTryTimes;
    }

    public Long getCourseTryTimes() 
    {
        return courseTryTimes;
    }
    public void setAuditStatus(Integer auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public Integer getAuditStatus() 
    {
        return auditStatus;
    }
    public void setSellStatus(Integer sellStatus) 
    {
        this.sellStatus = sellStatus;
    }

    public Integer getSellStatus() 
    {
        return sellStatus;
    }

    public Integer getCourseMedia() {
        return courseMedia;
    }

    public void setCourseMedia(Integer courseMedia) {
        this.courseMedia = courseMedia;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("courseId", getCourseId())
            .append("categoryId", getCategoryId())
            .append("courseName", getCourseName())
            .append("chaptersNum", getChaptersNum())
            .append("courseDescription", getCourseDescription())
            .append("courseImgs", getCourseImgs())
            .append("courseOldPrice", getCourseOldPrice())
            .append("coursePrice", getCoursePrice())
            .append("courseHot", getCourseHot())
            .append("courseNew", getCourseNew())
            .append("courseSales", getCourseSales())
            .append("courseTry", getCourseTry())
            .append("courseTryPages", getCourseTryPages())
            .append("courseTryTimes", getCourseTryTimes())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("auditStatus", getAuditStatus())
            .append("sellStatus", getSellStatus())
            .toString();
    }
}
