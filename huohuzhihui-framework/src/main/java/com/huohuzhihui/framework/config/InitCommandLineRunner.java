package com.huohuzhihui.framework.config;



import com.huohuzhihui.common.utils.DateUtils;
import org.apache.xmlbeans.GDate;
import org.smartboot.license.client.License;
import org.smartboot.license.client.LicenseEntity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.Date;


/**
 * License 配置
 */
@Component
public class InitCommandLineRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("InitCommandLineRunner...validate license");
        //File file=new File("license.txt");
        File file = ResourceUtils.getFile("classpath:license.txt");
        License license = new License();
        LicenseEntity licenseEntity=license.loadLicense(file);
        System.out.println("license expire time : "+ DateUtils.dateTime(new Date(licenseEntity.getExpireTime())));
    }
}
