package com.huohuzhihui.account.service;

import java.math.BigDecimal;
import java.util.List;

import com.huohuzhihui.account.domain.AccUserAccount;

/**
 * 账户Service接口
 * 
 * @author zylu
 * @date 2020-11-14
 */
public interface IAccUserAccountService 
{
    /**
     * 查询账户
     * 
     * @param id 账户ID
     * @return 账户
     */
    public AccUserAccount selectAccUserAccountById(Long id);

    /**
     * 查询账户列表
     * 
     * @param accUserAccount 账户
     * @return 账户集合
     */
    public List<AccUserAccount> selectAccUserAccountList(AccUserAccount accUserAccount);

    /**
     * 新增账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    public int insertAccUserAccount(AccUserAccount accUserAccount);

    /**
     * 修改账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    public int updateAccUserAccount(AccUserAccount accUserAccount);

    /**
     * 批量删除账户
     * 
     * @param ids 需要删除的账户ID
     * @return 结果
     */
    public int deleteAccUserAccountByIds(Long[] ids);

    /**
     * 删除账户信息
     * 
     * @param id 账户ID
     * @return 结果
     */
    public int deleteAccUserAccountById(Long id);

    /**
     * 冻结账户
     * @param ids 需要冻结账户的ID
     * @return 结果
     */
    public int freezeUserAccountByIds(Long[] ids);

    /**
     * 解冻账户
     * @param ids
     * @return
     */
    int unfreezeUserAccountByIds(Long[] ids);

    /**
     * 注销账户
     * @param ids
     * @return
     */
    int cancelUserAccountByIds(Long[] ids);


    /**
     * 账户充值
     * @param accountId 账户ID
     * @param amount 金额
     * @param source 来源
     * @param channelCode 支付渠道编码
     * @param createBy 创建人
     * @return
     */
    int recharge(Long accountId,  BigDecimal amount, String source,  String channelCode , String channelName ,String createBy);

    /**
     * 账户统计
     * @param account
     * @return
     */
    Long getUserAccountStatistics(AccUserAccount account);

    /**
     * 账户总余额
     * @return
     */
    BigDecimal getTotalUserBalance();

    public AccUserAccount selectAccUserAccountByUserId(Long userId);
}
