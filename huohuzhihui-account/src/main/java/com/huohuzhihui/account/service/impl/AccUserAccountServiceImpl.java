package com.huohuzhihui.account.service.impl;

import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.account.mapper.AccUserAccountMapper;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.common.utils.DateUtils;
import com.huohuzhihui.common.utils.SecurityUtils;
import com.huohuzhihui.common.utils.uuid.IdUtils;
import com.huohuzhihui.trade.service.ITradeOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 账户Service业务层处理
 * 
 * @author zylu
 * @date 2020-11-14
 */
@Service
public class AccUserAccountServiceImpl implements IAccUserAccountService 
{
    @Autowired
    private AccUserAccountMapper accUserAccountMapper;
    @Autowired
    private ITradeOrderService iTradeOrderService;



    /**
     * 查询账户
     * 
     * @param id 账户ID
     * @return 账户
     */
    @Override
    public AccUserAccount selectAccUserAccountById(Long id)
    {
        return accUserAccountMapper.selectAccUserAccountById(id);
    }

    /**
     * 查询账户列表
     * 
     * @param accUserAccount 账户
     * @return 账户
     */
    @Override
    public List<AccUserAccount> selectAccUserAccountList(AccUserAccount accUserAccount)
    {
        return accUserAccountMapper.selectAccUserAccountList(accUserAccount);
    }

    /**
     * 新增账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    @Override
    public int insertAccUserAccount(AccUserAccount accUserAccount)
    {
        accUserAccount.setCreateTime(DateUtils.getNowDate());
        return accUserAccountMapper.insertAccUserAccount(accUserAccount);
    }

    /**
     * 修改账户
     * 
     * @param accUserAccount 账户
     * @return 结果
     */
    @Override
    public int updateAccUserAccount(AccUserAccount accUserAccount)
    {
        accUserAccount.setUpdateTime(DateUtils.getNowDate());
        return accUserAccountMapper.updateAccUserAccount(accUserAccount);
    }

    /**
     * 批量删除账户
     * 
     * @param ids 需要删除的账户ID
     * @return 结果
     */
    @Override
    public int deleteAccUserAccountByIds(Long[] ids)
    {
        return accUserAccountMapper.deleteAccUserAccountByIds(ids);
    }

    /**
     * 删除账户信息
     * 
     * @param id 账户ID
     * @return 结果
     */
    @Override
    public int deleteAccUserAccountById(Long id)
    {
        return accUserAccountMapper.deleteAccUserAccountById(id);
    }

    /**
     * 冻结账户
     * @param ids 需要冻结账户的ID
     * @return
     */
    @Override
    public int freezeUserAccountByIds(Long[] ids) {

        return accUserAccountMapper.updateStatusByIds(1,DateUtils.getNowDate(),SecurityUtils.getUsername(),ids);
    }

    /**
     * 解冻账户
     * @param ids
     * @return
     */
    @Override
    public int unfreezeUserAccountByIds(Long[] ids) {
        return accUserAccountMapper.updateStatusByIds(0,DateUtils.getNowDate(),SecurityUtils.getUsername(),ids);
    }

    @Override
    public int cancelUserAccountByIds(Long[] ids) {
        return accUserAccountMapper.updateStatusByIds(2,DateUtils.getNowDate(),SecurityUtils.getUsername(),ids);
    }

    @Override
    public int recharge( Long accountId,  BigDecimal amount, String source,  String channelCode , String channelName,String createBy) {
        AccUserAccount accUserAccount = accUserAccountMapper.selectAccUserAccountById(accountId);
        if(accUserAccount != null) {
            String type = "0";//充值
            String accountName = accUserAccount.getUserName();
            Long deviceId = null;
            String cardNo = "";
            String tradeNo = IdUtils.fastSimpleUUID();
            iTradeOrderService.insertTradeOrder(type, accountId, accountName, amount, source, deviceId, channelCode, createBy,tradeNo);

            //更新当前账户余额
            AccUserAccount updateModel = new AccUserAccount();
            updateModel.setId(accUserAccount.getId());
            updateModel.setVersion(accUserAccount.getVersion());
            updateModel.setBalance(amount);//增加的金额
            updateModel.setUpdateBy(SecurityUtils.getUsername());
            updateModel.setUpdateTime(DateUtils.getNowDate());
            return accUserAccountMapper.recharge(updateModel);
        }
        return 0;
    }

    @Override
    public Long getUserAccountStatistics(AccUserAccount account) {
        return accUserAccountMapper.getUserAccountStatistics(account);
    }

    @Override
    public BigDecimal getTotalUserBalance() {
        return accUserAccountMapper.getTotalUserBalance();
    }

    @Override
    public AccUserAccount selectAccUserAccountByUserId(Long userId) {
        return accUserAccountMapper.selectAccUserAccountByUserId(userId);
    }
}
