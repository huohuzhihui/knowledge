package com.huohuzhihui.distribution.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 分润参数设置对象 dis_rank_param
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisRankParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 平台id */
    @Excel(name = "平台id")
    private String disPlatformId;

    /** 计算模型，如 百分比和固定金额 */
    @Excel(name = "计算模型，如 百分比和固定金额")
    private String calModel;

    /** 账户类型，如上级发展下级分润 ，交易分润。。。。 */
    @Excel(name = "账户类型，如上级发展下级分润 ，交易分润。。。。")
    private String accountType;

    /** 积分值 */
    @Excel(name = "积分值")
    private String disIntegralValue;

    /** 从下往上对应的级别关系 */
    @Excel(name = "从下往上对应的级别关系")
    private String disProLevel;

    /** 会员类型（1000：平台标示，其他为用户类型） */
    @Excel(name = "会员类型", readConverterExp = "1=000：平台标示，其他为用户类型")
    private String disUserType;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private String isDelete;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 用户段位（青铜等） */
    @Excel(name = "用户段位", readConverterExp = "青=铜等")
    private String disUserRank;

    /** 段位积分名称 */
    @Excel(name = "段位积分名称")
    private String disRankName;

    /** 身份类型(0,会员，1：平台商) */
    @Excel(name = "身份类型(0,会员，1：平台商)")
    private String identityType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDisPlatformId(String disPlatformId) 
    {
        this.disPlatformId = disPlatformId;
    }

    public String getDisPlatformId() 
    {
        return disPlatformId;
    }
    public void setCalModel(String calModel) 
    {
        this.calModel = calModel;
    }

    public String getCalModel() 
    {
        return calModel;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setDisIntegralValue(String disIntegralValue) 
    {
        this.disIntegralValue = disIntegralValue;
    }

    public String getDisIntegralValue() 
    {
        return disIntegralValue;
    }
    public void setDisProLevel(String disProLevel) 
    {
        this.disProLevel = disProLevel;
    }

    public String getDisProLevel() 
    {
        return disProLevel;
    }
    public void setDisUserType(String disUserType) 
    {
        this.disUserType = disUserType;
    }

    public String getDisUserType() 
    {
        return disUserType;
    }
    public void setIsDelete(String isDelete) 
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete() 
    {
        return isDelete;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setDisUserRank(String disUserRank) 
    {
        this.disUserRank = disUserRank;
    }

    public String getDisUserRank() 
    {
        return disUserRank;
    }
    public void setDisRankName(String disRankName) 
    {
        this.disRankName = disRankName;
    }

    public String getDisRankName() 
    {
        return disRankName;
    }
    public void setIdentityType(String identityType) 
    {
        this.identityType = identityType;
    }

    public String getIdentityType() 
    {
        return identityType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disPlatformId", getDisPlatformId())
            .append("calModel", getCalModel())
            .append("accountType", getAccountType())
            .append("disIntegralValue", getDisIntegralValue())
            .append("disProLevel", getDisProLevel())
            .append("disUserType", getDisUserType())
            .append("isDelete", getIsDelete())
            .append("updateTime", getUpdateTime())
            .append("addTime", getAddTime())
            .append("disUserRank", getDisUserRank())
            .append("disRankName", getDisRankName())
            .append("identityType", getIdentityType())
            .toString();
    }
}
