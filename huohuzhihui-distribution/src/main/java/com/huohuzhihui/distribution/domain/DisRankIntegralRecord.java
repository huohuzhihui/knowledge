package com.huohuzhihui.distribution.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 系统积分记录对象 dis_rank_integral_record
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisRankIntegralRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** 系统积分 */
    @Excel(name = "系统积分")
    private Integer sysIntegral;

    /** 是否使用（Y:使用,N未使用） */
    @Excel(name = "是否使用", readConverterExp = "Y=:使用,N未使用")
    private String isUse;

    /** 是否过期(Y:已过期，N未过期) 暂时不用 */
    @Excel(name = "是否过期(Y:已过期，N未过期) 暂时不用")
    private String isExpire;

    /** 使用前积分 */
    @Excel(name = "使用前积分")
    private Integer beforeIntegral;

    /** 使用后积分 */
    @Excel(name = "使用后积分")
    private Integer afterIntegral;

    /** 到期时间（暂时不用） */
    @Excel(name = "到期时间", readConverterExp = "暂=时不用")
    private String expireTime;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 来源(交易、升级，下级升级) */
    @Excel(name = "来源(交易、升级，下级升级)")
    private String accountType;

    /** 来源用户id */
    @Excel(name = "来源用户id")
    private String sourceUserId;

    /** 来源备注 */
    @Excel(name = "来源备注")
    private String sourceRemak;

    /** 使用时间 */
    @Excel(name = "使用时间")
    private String useTime;

    /** 使用备注 */
    @Excel(name = "使用备注")
    private String useRemark;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setSysIntegral(Integer sysIntegral) 
    {
        this.sysIntegral = sysIntegral;
    }

    public Integer getSysIntegral() 
    {
        return sysIntegral;
    }
    public void setIsUse(String isUse) 
    {
        this.isUse = isUse;
    }

    public String getIsUse() 
    {
        return isUse;
    }
    public void setIsExpire(String isExpire) 
    {
        this.isExpire = isExpire;
    }

    public String getIsExpire() 
    {
        return isExpire;
    }
    public void setBeforeIntegral(Integer beforeIntegral) 
    {
        this.beforeIntegral = beforeIntegral;
    }

    public Integer getBeforeIntegral() 
    {
        return beforeIntegral;
    }
    public void setAfterIntegral(Integer afterIntegral) 
    {
        this.afterIntegral = afterIntegral;
    }

    public Integer getAfterIntegral() 
    {
        return afterIntegral;
    }
    public void setExpireTime(String expireTime) 
    {
        this.expireTime = expireTime;
    }

    public String getExpireTime() 
    {
        return expireTime;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setSourceUserId(String sourceUserId) 
    {
        this.sourceUserId = sourceUserId;
    }

    public String getSourceUserId() 
    {
        return sourceUserId;
    }
    public void setSourceRemak(String sourceRemak) 
    {
        this.sourceRemak = sourceRemak;
    }

    public String getSourceRemak() 
    {
        return sourceRemak;
    }
    public void setUseTime(String useTime) 
    {
        this.useTime = useTime;
    }

    public String getUseTime() 
    {
        return useTime;
    }
    public void setUseRemark(String useRemark) 
    {
        this.useRemark = useRemark;
    }

    public String getUseRemark() 
    {
        return useRemark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("sysIntegral", getSysIntegral())
            .append("isUse", getIsUse())
            .append("isExpire", getIsExpire())
            .append("beforeIntegral", getBeforeIntegral())
            .append("afterIntegral", getAfterIntegral())
            .append("expireTime", getExpireTime())
            .append("addTime", getAddTime())
            .append("accountType", getAccountType())
            .append("sourceUserId", getSourceUserId())
            .append("sourceRemak", getSourceRemak())
            .append("useTime", getUseTime())
            .append("useRemark", getUseRemark())
            .toString();
    }
}
