package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 提现对象 dis_withdraw_record
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisWithdrawRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** 提现单号 */
    @Excel(name = "提现单号")
    private String withdrawNum;

    /** 提现金额 */
    @Excel(name = "提现金额")
    private BigDecimal totalAmount;

    /** 手续费 */
    @Excel(name = "手续费")
    private BigDecimal feeAmount;

    /** 实际到账金额 */
    @Excel(name = "实际到账金额")
    private BigDecimal realAmount;

    /** 提现时间 */
    @Excel(name = "提现时间")
    private String withdrawTime;

    /** 处理时间 */
    @Excel(name = "处理时间")
    private String handleTime;

    /** 处理状态 */
    @Excel(name = "处理状态")
    private String withdrawStatus;

    /** 提现账户 */
    @Excel(name = "提现账户")
    private String accountType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setWithdrawNum(String withdrawNum) 
    {
        this.withdrawNum = withdrawNum;
    }

    public String getWithdrawNum() 
    {
        return withdrawNum;
    }
    public void setTotalAmount(BigDecimal totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() 
    {
        return totalAmount;
    }
    public void setFeeAmount(BigDecimal feeAmount) 
    {
        this.feeAmount = feeAmount;
    }

    public BigDecimal getFeeAmount() 
    {
        return feeAmount;
    }
    public void setRealAmount(BigDecimal realAmount) 
    {
        this.realAmount = realAmount;
    }

    public BigDecimal getRealAmount() 
    {
        return realAmount;
    }
    public void setWithdrawTime(String withdrawTime) 
    {
        this.withdrawTime = withdrawTime;
    }

    public String getWithdrawTime() 
    {
        return withdrawTime;
    }
    public void setHandleTime(String handleTime) 
    {
        this.handleTime = handleTime;
    }

    public String getHandleTime() 
    {
        return handleTime;
    }
    public void setWithdrawStatus(String withdrawStatus) 
    {
        this.withdrawStatus = withdrawStatus;
    }

    public String getWithdrawStatus() 
    {
        return withdrawStatus;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("withdrawNum", getWithdrawNum())
            .append("totalAmount", getTotalAmount())
            .append("feeAmount", getFeeAmount())
            .append("realAmount", getRealAmount())
            .append("withdrawTime", getWithdrawTime())
            .append("handleTime", getHandleTime())
            .append("withdrawStatus", getWithdrawStatus())
            .append("accountType", getAccountType())
            .toString();
    }
}
