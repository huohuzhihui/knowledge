package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 提现收费配置对象 dist_withdraw_param
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DistWithdrawParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 开始金额 */
    @Excel(name = "开始金额")
    private BigDecimal beginAmount;

    /** 结束金额 */
    @Excel(name = "结束金额")
    private BigDecimal endAmount;

    /** 计算模型，如 百分比和固定金额 */
    @Excel(name = "计算模型，如 百分比和固定金额")
    private String calModel;

    /** 提现值 */
    @Excel(name = "提现值")
    private String disWithdrawValue;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private String isDelete;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBeginAmount(BigDecimal beginAmount) 
    {
        this.beginAmount = beginAmount;
    }

    public BigDecimal getBeginAmount() 
    {
        return beginAmount;
    }
    public void setEndAmount(BigDecimal endAmount) 
    {
        this.endAmount = endAmount;
    }

    public BigDecimal getEndAmount() 
    {
        return endAmount;
    }
    public void setCalModel(String calModel) 
    {
        this.calModel = calModel;
    }

    public String getCalModel() 
    {
        return calModel;
    }
    public void setDisWithdrawValue(String disWithdrawValue) 
    {
        this.disWithdrawValue = disWithdrawValue;
    }

    public String getDisWithdrawValue() 
    {
        return disWithdrawValue;
    }
    public void setIsDelete(String isDelete) 
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete() 
    {
        return isDelete;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("beginAmount", getBeginAmount())
            .append("endAmount", getEndAmount())
            .append("calModel", getCalModel())
            .append("disWithdrawValue", getDisWithdrawValue())
            .append("isDelete", getIsDelete())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
