package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 交易金额记录对象 dis_trade_record
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisTradeRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** 第三方订单号 */
    @Excel(name = "第三方订单号")
    private String tradeNum;

    /** 交易金额 */
    @Excel(name = "交易金额")
    private BigDecimal tradeAmount;

    /** 交易时间 */
    @Excel(name = "交易时间")
    private String tradeTime;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setTradeNum(String tradeNum) 
    {
        this.tradeNum = tradeNum;
    }

    public String getTradeNum() 
    {
        return tradeNum;
    }
    public void setTradeAmount(BigDecimal tradeAmount) 
    {
        this.tradeAmount = tradeAmount;
    }

    public BigDecimal getTradeAmount() 
    {
        return tradeAmount;
    }
    public void setTradeTime(String tradeTime) 
    {
        this.tradeTime = tradeTime;
    }

    public String getTradeTime() 
    {
        return tradeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("tradeNum", getTradeNum())
            .append("tradeAmount", getTradeAmount())
            .append("tradeTime", getTradeTime())
            .toString();
    }
}
