package com.huohuzhihui.distribution.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 垂直升级配置对象 dis_upgrade_param
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisUpgradeParam extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String upgradeName;

    /** 开始金额 */
    @Excel(name = "开始金额")
    private Integer beginIntegral;

    /** 结束金额 */
    @Excel(name = "结束金额")
    private Integer endIntegral;

    /** 用户水平等级 */
    @Excel(name = "用户水平等级")
    private String disUserRank;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private String isDelete;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 0会员  1代理商 */
    @Excel(name = "0会员  1代理商")
    private String identityType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUpgradeName(String upgradeName) 
    {
        this.upgradeName = upgradeName;
    }

    public String getUpgradeName() 
    {
        return upgradeName;
    }
    public void setBeginIntegral(Integer beginIntegral) 
    {
        this.beginIntegral = beginIntegral;
    }

    public Integer getBeginIntegral() 
    {
        return beginIntegral;
    }
    public void setEndIntegral(Integer endIntegral) 
    {
        this.endIntegral = endIntegral;
    }

    public Integer getEndIntegral() 
    {
        return endIntegral;
    }
    public void setDisUserRank(String disUserRank) 
    {
        this.disUserRank = disUserRank;
    }

    public String getDisUserRank() 
    {
        return disUserRank;
    }
    public void setIsDelete(String isDelete) 
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete() 
    {
        return isDelete;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setIdentityType(String identityType) 
    {
        this.identityType = identityType;
    }

    public String getIdentityType() 
    {
        return identityType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("upgradeName", getUpgradeName())
            .append("beginIntegral", getBeginIntegral())
            .append("endIntegral", getEndIntegral())
            .append("disUserRank", getDisUserRank())
            .append("isDelete", getIsDelete())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("identityType", getIdentityType())
            .toString();
    }
}
