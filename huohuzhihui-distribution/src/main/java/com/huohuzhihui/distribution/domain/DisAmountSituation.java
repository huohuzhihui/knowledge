package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 账户变动，用于记录账户变动情况对象 dis_amount_situation
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisAmountSituation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private String disUserId;

    /** (0:收入,1:支出) */
    @Excel(name = "(0:收入,1:支出)")
    private String type;

    /** 总账户变动之前余额 */
    @Excel(name = "总账户变动之前余额")
    private BigDecimal beforeChangeAmount;

    /** 总账户变动之后余额 */
    @Excel(name = "总账户变动之后余额")
    private BigDecimal afterChangeAmount;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 具体账户变动之前金额 */
    @Excel(name = "具体账户变动之前金额")
    private BigDecimal specificBeforeChangeAmount;

    /** 具体账户变动之后金额 */
    @Excel(name = "具体账户变动之后金额")
    private BigDecimal specificAfterChangeAmount;

    /** 具体账户类型 */
    @Excel(name = "具体账户类型")
    private String accountType;

    /** 账户变动金额 */
    @Excel(name = "账户变动金额")
    private BigDecimal changeAmount;

    /** 具体变动描述 */
    @Excel(name = "具体变动描述")
    private String describe;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setBeforeChangeAmount(BigDecimal beforeChangeAmount) 
    {
        this.beforeChangeAmount = beforeChangeAmount;
    }

    public BigDecimal getBeforeChangeAmount() 
    {
        return beforeChangeAmount;
    }
    public void setAfterChangeAmount(BigDecimal afterChangeAmount) 
    {
        this.afterChangeAmount = afterChangeAmount;
    }

    public BigDecimal getAfterChangeAmount() 
    {
        return afterChangeAmount;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setSpecificBeforeChangeAmount(BigDecimal specificBeforeChangeAmount) 
    {
        this.specificBeforeChangeAmount = specificBeforeChangeAmount;
    }

    public BigDecimal getSpecificBeforeChangeAmount() 
    {
        return specificBeforeChangeAmount;
    }
    public void setSpecificAfterChangeAmount(BigDecimal specificAfterChangeAmount) 
    {
        this.specificAfterChangeAmount = specificAfterChangeAmount;
    }

    public BigDecimal getSpecificAfterChangeAmount() 
    {
        return specificAfterChangeAmount;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setChangeAmount(BigDecimal changeAmount) 
    {
        this.changeAmount = changeAmount;
    }

    public BigDecimal getChangeAmount() 
    {
        return changeAmount;
    }
    public void setDescribe(String describe) 
    {
        this.describe = describe;
    }

    public String getDescribe() 
    {
        return describe;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("type", getType())
            .append("beforeChangeAmount", getBeforeChangeAmount())
            .append("afterChangeAmount", getAfterChangeAmount())
            .append("addTime", getAddTime())
            .append("specificBeforeChangeAmount", getSpecificBeforeChangeAmount())
            .append("specificAfterChangeAmount", getSpecificAfterChangeAmount())
            .append("accountType", getAccountType())
            .append("changeAmount", getChangeAmount())
            .append("describe", getDescribe())
            .toString();
    }
}
