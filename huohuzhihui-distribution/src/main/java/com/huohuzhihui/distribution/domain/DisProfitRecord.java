package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 分润记录对象 dis_profit_record
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisProfitRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 收入编号 */
    @Excel(name = "收入编号")
    private String profitNum;

    /** $column.columnComment */
    @Excel(name = "收入编号")
    private String disPlatformId;

    /** $column.columnComment */
    @Excel(name = "收入编号")
    private String disGetUserId;

    /** $column.columnComment */
    @Excel(name = "收入编号")
    private String disSetUserId;

    /** $column.columnComment */
    @Excel(name = "收入编号")
    private BigDecimal disAmount;

    /** 账户类型 */
    @Excel(name = "账户类型")
    private String accountType;

    /** 备注 */
    @Excel(name = "备注")
    private String disNote;

    /** $column.columnComment */
    @Excel(name = "备注")
    private String disUserType;

    /** 对应第三方订单编号 */
    @Excel(name = "对应第三方订单编号")
    private String disOrderId;

    /** $column.columnComment */
    @Excel(name = "对应第三方订单编号")
    private String isDelete;

    /** $column.columnComment */
    @Excel(name = "对应第三方订单编号")
    private String addTime;

    /** 分类（0：用户分润 1:平台分润） */
    @Excel(name = "分类", readConverterExp = "0=：用户分润,1=:平台分润")
    private String identityType;

    /** 交易前金额 */
    @Excel(name = "交易前金额")
    private BigDecimal beforeAmount;

    /** 交易后金额 */
    @Excel(name = "交易后金额")
    private BigDecimal afterAmount;

    /** 交易类型交易前金额 */
    @Excel(name = "交易类型交易前金额")
    private BigDecimal beforeProAmount;

    /** 交易类型交易后金额 */
    @Excel(name = "交易类型交易后金额")
    private BigDecimal afterProAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProfitNum(String profitNum) 
    {
        this.profitNum = profitNum;
    }

    public String getProfitNum() 
    {
        return profitNum;
    }
    public void setDisPlatformId(String disPlatformId) 
    {
        this.disPlatformId = disPlatformId;
    }

    public String getDisPlatformId() 
    {
        return disPlatformId;
    }
    public void setDisGetUserId(String disGetUserId) 
    {
        this.disGetUserId = disGetUserId;
    }

    public String getDisGetUserId() 
    {
        return disGetUserId;
    }
    public void setDisSetUserId(String disSetUserId) 
    {
        this.disSetUserId = disSetUserId;
    }

    public String getDisSetUserId() 
    {
        return disSetUserId;
    }
    public void setDisAmount(BigDecimal disAmount) 
    {
        this.disAmount = disAmount;
    }

    public BigDecimal getDisAmount() 
    {
        return disAmount;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setDisNote(String disNote) 
    {
        this.disNote = disNote;
    }

    public String getDisNote() 
    {
        return disNote;
    }
    public void setDisUserType(String disUserType) 
    {
        this.disUserType = disUserType;
    }

    public String getDisUserType() 
    {
        return disUserType;
    }
    public void setDisOrderId(String disOrderId) 
    {
        this.disOrderId = disOrderId;
    }

    public String getDisOrderId() 
    {
        return disOrderId;
    }
    public void setIsDelete(String isDelete) 
    {
        this.isDelete = isDelete;
    }

    public String getIsDelete() 
    {
        return isDelete;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setIdentityType(String identityType) 
    {
        this.identityType = identityType;
    }

    public String getIdentityType() 
    {
        return identityType;
    }
    public void setBeforeAmount(BigDecimal beforeAmount) 
    {
        this.beforeAmount = beforeAmount;
    }

    public BigDecimal getBeforeAmount() 
    {
        return beforeAmount;
    }
    public void setAfterAmount(BigDecimal afterAmount) 
    {
        this.afterAmount = afterAmount;
    }

    public BigDecimal getAfterAmount() 
    {
        return afterAmount;
    }
    public void setBeforeProAmount(BigDecimal beforeProAmount) 
    {
        this.beforeProAmount = beforeProAmount;
    }

    public BigDecimal getBeforeProAmount() 
    {
        return beforeProAmount;
    }
    public void setAfterProAmount(BigDecimal afterProAmount) 
    {
        this.afterProAmount = afterProAmount;
    }

    public BigDecimal getAfterProAmount() 
    {
        return afterProAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("profitNum", getProfitNum())
            .append("disPlatformId", getDisPlatformId())
            .append("disGetUserId", getDisGetUserId())
            .append("disSetUserId", getDisSetUserId())
            .append("disAmount", getDisAmount())
            .append("accountType", getAccountType())
            .append("disNote", getDisNote())
            .append("disUserType", getDisUserType())
            .append("disOrderId", getDisOrderId())
            .append("isDelete", getIsDelete())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("identityType", getIdentityType())
            .append("beforeAmount", getBeforeAmount())
            .append("afterAmount", getAfterAmount())
            .append("beforeProAmount", getBeforeProAmount())
            .append("afterProAmount", getAfterProAmount())
            .toString();
    }
}
