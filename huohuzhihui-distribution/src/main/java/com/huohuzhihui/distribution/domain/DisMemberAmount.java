package com.huohuzhihui.distribution.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;

/**
 * 账户金额对象 dis_member_amount
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public class DisMemberAmount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户标示id */
    @Excel(name = "用户标示id")
    private String disUserId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String disUserName;

    /** 总金额 */
    @Excel(name = "总金额")
    private BigDecimal totalAmount;

    /** 冻结金额 */
    @Excel(name = "冻结金额")
    private BigDecimal frozenAmount;

    /** 可用金额 */
    @Excel(name = "可用金额")
    private BigDecimal avaibleAmount;

    /** 类型（0会员 1 代理商） */
    @Excel(name = "类型", readConverterExp = "0=会员,1=,代=理商")
    private String identityType;

    /** 添加时间 */
    @Excel(name = "添加时间")
    private String addTime;

    /** 状态（0正常 1冻结） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=冻结")
    private String amountStatus;

    /** 扩展字段，交易账户总金额 */
    @Excel(name = "扩展字段，交易账户总金额")
    private BigDecimal tradeTotalAmount;

    /** 扩展字段，交易账户冻结金额 */
    @Excel(name = "扩展字段，交易账户冻结金额")
    private BigDecimal tradeFrozenAmount;

    /** 扩展字段，交易账户可用金额 */
    @Excel(name = "扩展字段，交易账户可用金额")
    private BigDecimal tradeAvaibleAmount;

    /** 扩展字段，等级账户总金额 */
    @Excel(name = "扩展字段，等级账户总金额")
    private BigDecimal levelTotalAmount;

    /** 扩展字段，等级账户冻结金额 */
    @Excel(name = "扩展字段，等级账户冻结金额")
    private BigDecimal levelFrozenAmount;

    /** 扩展字段，等级账户可用金额 */
    @Excel(name = "扩展字段，等级账户可用金额")
    private BigDecimal levelAvaibleAmount;

    /** 扩展字段，邀请用户总积分 */
    @Excel(name = "扩展字段，邀请用户总积分")
    private BigDecimal inviteTotalAmount;

    /** 扩展字段，邀请用户冻结积分 */
    @Excel(name = "扩展字段，邀请用户冻结积分")
    private BigDecimal inviteFrozenAmount;

    /** 扩展字段，邀请用户可用积分 */
    @Excel(name = "扩展字段，邀请用户可用积分")
    private BigDecimal inviteAvaibleAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDisUserId(String disUserId) 
    {
        this.disUserId = disUserId;
    }

    public String getDisUserId() 
    {
        return disUserId;
    }
    public void setDisUserName(String disUserName) 
    {
        this.disUserName = disUserName;
    }

    public String getDisUserName() 
    {
        return disUserName;
    }
    public void setTotalAmount(BigDecimal totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() 
    {
        return totalAmount;
    }
    public void setFrozenAmount(BigDecimal frozenAmount) 
    {
        this.frozenAmount = frozenAmount;
    }

    public BigDecimal getFrozenAmount() 
    {
        return frozenAmount;
    }
    public void setAvaibleAmount(BigDecimal avaibleAmount) 
    {
        this.avaibleAmount = avaibleAmount;
    }

    public BigDecimal getAvaibleAmount() 
    {
        return avaibleAmount;
    }
    public void setIdentityType(String identityType) 
    {
        this.identityType = identityType;
    }

    public String getIdentityType() 
    {
        return identityType;
    }
    public void setAddTime(String addTime) 
    {
        this.addTime = addTime;
    }

    public String getAddTime() 
    {
        return addTime;
    }
    public void setAmountStatus(String amountStatus) 
    {
        this.amountStatus = amountStatus;
    }

    public String getAmountStatus() 
    {
        return amountStatus;
    }
    public void setTradeTotalAmount(BigDecimal tradeTotalAmount) 
    {
        this.tradeTotalAmount = tradeTotalAmount;
    }

    public BigDecimal getTradeTotalAmount() 
    {
        return tradeTotalAmount;
    }
    public void setTradeFrozenAmount(BigDecimal tradeFrozenAmount) 
    {
        this.tradeFrozenAmount = tradeFrozenAmount;
    }

    public BigDecimal getTradeFrozenAmount() 
    {
        return tradeFrozenAmount;
    }
    public void setTradeAvaibleAmount(BigDecimal tradeAvaibleAmount) 
    {
        this.tradeAvaibleAmount = tradeAvaibleAmount;
    }

    public BigDecimal getTradeAvaibleAmount() 
    {
        return tradeAvaibleAmount;
    }
    public void setLevelTotalAmount(BigDecimal levelTotalAmount) 
    {
        this.levelTotalAmount = levelTotalAmount;
    }

    public BigDecimal getLevelTotalAmount() 
    {
        return levelTotalAmount;
    }
    public void setLevelFrozenAmount(BigDecimal levelFrozenAmount) 
    {
        this.levelFrozenAmount = levelFrozenAmount;
    }

    public BigDecimal getLevelFrozenAmount() 
    {
        return levelFrozenAmount;
    }
    public void setLevelAvaibleAmount(BigDecimal levelAvaibleAmount) 
    {
        this.levelAvaibleAmount = levelAvaibleAmount;
    }

    public BigDecimal getLevelAvaibleAmount() 
    {
        return levelAvaibleAmount;
    }
    public void setInviteTotalAmount(BigDecimal inviteTotalAmount) 
    {
        this.inviteTotalAmount = inviteTotalAmount;
    }

    public BigDecimal getInviteTotalAmount() 
    {
        return inviteTotalAmount;
    }
    public void setInviteFrozenAmount(BigDecimal inviteFrozenAmount) 
    {
        this.inviteFrozenAmount = inviteFrozenAmount;
    }

    public BigDecimal getInviteFrozenAmount() 
    {
        return inviteFrozenAmount;
    }
    public void setInviteAvaibleAmount(BigDecimal inviteAvaibleAmount) 
    {
        this.inviteAvaibleAmount = inviteAvaibleAmount;
    }

    public BigDecimal getInviteAvaibleAmount() 
    {
        return inviteAvaibleAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("disUserId", getDisUserId())
            .append("disUserName", getDisUserName())
            .append("totalAmount", getTotalAmount())
            .append("frozenAmount", getFrozenAmount())
            .append("avaibleAmount", getAvaibleAmount())
            .append("identityType", getIdentityType())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("amountStatus", getAmountStatus())
            .append("tradeTotalAmount", getTradeTotalAmount())
            .append("tradeFrozenAmount", getTradeFrozenAmount())
            .append("tradeAvaibleAmount", getTradeAvaibleAmount())
            .append("levelTotalAmount", getLevelTotalAmount())
            .append("levelFrozenAmount", getLevelFrozenAmount())
            .append("levelAvaibleAmount", getLevelAvaibleAmount())
            .append("inviteTotalAmount", getInviteTotalAmount())
            .append("inviteFrozenAmount", getInviteFrozenAmount())
            .append("inviteAvaibleAmount", getInviteAvaibleAmount())
            .toString();
    }
}
