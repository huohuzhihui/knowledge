package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisMemberInfo;
import com.huohuzhihui.distribution.service.IDisMemberInfoService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 用户Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/info")
public class DisMemberInfoController extends BaseController
{
    @Autowired
    private IDisMemberInfoService disMemberInfoService;

    /**
     * 查询用户列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisMemberInfo disMemberInfo)
    {
        startPage();
        List<DisMemberInfo> list = disMemberInfoService.selectDisMemberInfoList(disMemberInfo);
        return getDataTable(list);
    }

    /**
     * 导出用户列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:export')")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisMemberInfo disMemberInfo)
    {
        List<DisMemberInfo> list = disMemberInfoService.selectDisMemberInfoList(disMemberInfo);
        ExcelUtil<DisMemberInfo> util = new ExcelUtil<DisMemberInfo>(DisMemberInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 获取用户详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disMemberInfoService.selectDisMemberInfoById(id));
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:add')")
    @Log(title = "用户", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisMemberInfo disMemberInfo)
    {
        return toAjax(disMemberInfoService.insertDisMemberInfo(disMemberInfo));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:edit')")
    @Log(title = "用户", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisMemberInfo disMemberInfo)
    {
        return toAjax(disMemberInfoService.updateDisMemberInfo(disMemberInfo));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('distribution:info:remove')")
    @Log(title = "用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disMemberInfoService.deleteDisMemberInfoByIds(ids));
    }
}
