package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisMemberAmount;
import com.huohuzhihui.distribution.service.IDisMemberAmountService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 账户金额Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/amount")
public class DisMemberAmountController extends BaseController
{
    @Autowired
    private IDisMemberAmountService disMemberAmountService;

    /**
     * 查询账户金额列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisMemberAmount disMemberAmount)
    {
        startPage();
        List<DisMemberAmount> list = disMemberAmountService.selectDisMemberAmountList(disMemberAmount);
        return getDataTable(list);
    }

    /**
     * 导出账户金额列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:export')")
    @Log(title = "账户金额", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisMemberAmount disMemberAmount)
    {
        List<DisMemberAmount> list = disMemberAmountService.selectDisMemberAmountList(disMemberAmount);
        ExcelUtil<DisMemberAmount> util = new ExcelUtil<DisMemberAmount>(DisMemberAmount.class);
        return util.exportExcel(list, "amount");
    }

    /**
     * 获取账户金额详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disMemberAmountService.selectDisMemberAmountById(id));
    }

    /**
     * 新增账户金额
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:add')")
    @Log(title = "账户金额", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisMemberAmount disMemberAmount)
    {
        return toAjax(disMemberAmountService.insertDisMemberAmount(disMemberAmount));
    }

    /**
     * 修改账户金额
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:edit')")
    @Log(title = "账户金额", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisMemberAmount disMemberAmount)
    {
        return toAjax(disMemberAmountService.updateDisMemberAmount(disMemberAmount));
    }

    /**
     * 删除账户金额
     */
    @PreAuthorize("@ss.hasPermi('distribution:amount:remove')")
    @Log(title = "账户金额", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disMemberAmountService.deleteDisMemberAmountByIds(ids));
    }
}
