package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisProfitRecord;
import com.huohuzhihui.distribution.service.IDisProfitRecordService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 分润记录Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/profitRecord")
public class DisProfitRecordController extends BaseController
{
    @Autowired
    private IDisProfitRecordService disProfitRecordService;

    /**
     * 查询分润记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisProfitRecord disProfitRecord)
    {
        startPage();
        List<DisProfitRecord> list = disProfitRecordService.selectDisProfitRecordList(disProfitRecord);
        return getDataTable(list);
    }

    /**
     * 导出分润记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:export')")
    @Log(title = "分润记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisProfitRecord disProfitRecord)
    {
        List<DisProfitRecord> list = disProfitRecordService.selectDisProfitRecordList(disProfitRecord);
        ExcelUtil<DisProfitRecord> util = new ExcelUtil<DisProfitRecord>(DisProfitRecord.class);
        return util.exportExcel(list, "profitRecord");
    }

    /**
     * 获取分润记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disProfitRecordService.selectDisProfitRecordById(id));
    }

    /**
     * 新增分润记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:add')")
    @Log(title = "分润记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisProfitRecord disProfitRecord)
    {
        return toAjax(disProfitRecordService.insertDisProfitRecord(disProfitRecord));
    }

    /**
     * 修改分润记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:edit')")
    @Log(title = "分润记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisProfitRecord disProfitRecord)
    {
        return toAjax(disProfitRecordService.updateDisProfitRecord(disProfitRecord));
    }

    /**
     * 删除分润记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:profitRecord:remove')")
    @Log(title = "分润记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disProfitRecordService.deleteDisProfitRecordByIds(ids));
    }
}
