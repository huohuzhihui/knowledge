package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisRankIntegralRecord;
import com.huohuzhihui.distribution.service.IDisRankIntegralRecordService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 系统积分记录Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/rankIntegralRecord")
public class DisRankIntegralRecordController extends BaseController
{
    @Autowired
    private IDisRankIntegralRecordService disRankIntegralRecordService;

    /**
     * 查询系统积分记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisRankIntegralRecord disRankIntegralRecord)
    {
        startPage();
        List<DisRankIntegralRecord> list = disRankIntegralRecordService.selectDisRankIntegralRecordList(disRankIntegralRecord);
        return getDataTable(list);
    }

    /**
     * 导出系统积分记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:export')")
    @Log(title = "系统积分记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisRankIntegralRecord disRankIntegralRecord)
    {
        List<DisRankIntegralRecord> list = disRankIntegralRecordService.selectDisRankIntegralRecordList(disRankIntegralRecord);
        ExcelUtil<DisRankIntegralRecord> util = new ExcelUtil<DisRankIntegralRecord>(DisRankIntegralRecord.class);
        return util.exportExcel(list, "rankIntegralRecord");
    }

    /**
     * 获取系统积分记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(disRankIntegralRecordService.selectDisRankIntegralRecordById(id));
    }

    /**
     * 新增系统积分记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:add')")
    @Log(title = "系统积分记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisRankIntegralRecord disRankIntegralRecord)
    {
        return toAjax(disRankIntegralRecordService.insertDisRankIntegralRecord(disRankIntegralRecord));
    }

    /**
     * 修改系统积分记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:edit')")
    @Log(title = "系统积分记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisRankIntegralRecord disRankIntegralRecord)
    {
        return toAjax(disRankIntegralRecordService.updateDisRankIntegralRecord(disRankIntegralRecord));
    }

    /**
     * 删除系统积分记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankIntegralRecord:remove')")
    @Log(title = "系统积分记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(disRankIntegralRecordService.deleteDisRankIntegralRecordByIds(ids));
    }
}
