package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisAmountSituation;
import com.huohuzhihui.distribution.service.IDisAmountSituationService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 账户变动，用于记录账户变动情况Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/situation")
public class DisAmountSituationController extends BaseController
{
    @Autowired
    private IDisAmountSituationService disAmountSituationService;

    /**
     * 查询账户变动，用于记录账户变动情况列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisAmountSituation disAmountSituation)
    {
        startPage();
        List<DisAmountSituation> list = disAmountSituationService.selectDisAmountSituationList(disAmountSituation);
        return getDataTable(list);
    }

    /**
     * 导出账户变动，用于记录账户变动情况列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:export')")
    @Log(title = "账户变动，用于记录账户变动情况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisAmountSituation disAmountSituation)
    {
        List<DisAmountSituation> list = disAmountSituationService.selectDisAmountSituationList(disAmountSituation);
        ExcelUtil<DisAmountSituation> util = new ExcelUtil<DisAmountSituation>(DisAmountSituation.class);
        return util.exportExcel(list, "situation");
    }

    /**
     * 获取账户变动，用于记录账户变动情况详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disAmountSituationService.selectDisAmountSituationById(id));
    }

    /**
     * 新增账户变动，用于记录账户变动情况
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:add')")
    @Log(title = "账户变动，用于记录账户变动情况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisAmountSituation disAmountSituation)
    {
        return toAjax(disAmountSituationService.insertDisAmountSituation(disAmountSituation));
    }

    /**
     * 修改账户变动，用于记录账户变动情况
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:edit')")
    @Log(title = "账户变动，用于记录账户变动情况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisAmountSituation disAmountSituation)
    {
        return toAjax(disAmountSituationService.updateDisAmountSituation(disAmountSituation));
    }

    /**
     * 删除账户变动，用于记录账户变动情况
     */
    @PreAuthorize("@ss.hasPermi('distribution:situation:remove')")
    @Log(title = "账户变动，用于记录账户变动情况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disAmountSituationService.deleteDisAmountSituationByIds(ids));
    }
}
