package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisRankParam;
import com.huohuzhihui.distribution.service.IDisRankParamService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 分润参数设置Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/rankParam")
public class DisRankParamController extends BaseController
{
    @Autowired
    private IDisRankParamService disRankParamService;

    /**
     * 查询分润参数设置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisRankParam disRankParam)
    {
        startPage();
        List<DisRankParam> list = disRankParamService.selectDisRankParamList(disRankParam);
        return getDataTable(list);
    }

    /**
     * 导出分润参数设置列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:export')")
    @Log(title = "分润参数设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisRankParam disRankParam)
    {
        List<DisRankParam> list = disRankParamService.selectDisRankParamList(disRankParam);
        ExcelUtil<DisRankParam> util = new ExcelUtil<DisRankParam>(DisRankParam.class);
        return util.exportExcel(list, "rankParam");
    }

    /**
     * 获取分润参数设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(disRankParamService.selectDisRankParamById(id));
    }

    /**
     * 新增分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:add')")
    @Log(title = "分润参数设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisRankParam disRankParam)
    {
        return toAjax(disRankParamService.insertDisRankParam(disRankParam));
    }

    /**
     * 修改分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:edit')")
    @Log(title = "分润参数设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisRankParam disRankParam)
    {
        return toAjax(disRankParamService.updateDisRankParam(disRankParam));
    }

    /**
     * 删除分润参数设置
     */
    @PreAuthorize("@ss.hasPermi('distribution:rankParam:remove')")
    @Log(title = "分润参数设置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(disRankParamService.deleteDisRankParamByIds(ids));
    }
}
