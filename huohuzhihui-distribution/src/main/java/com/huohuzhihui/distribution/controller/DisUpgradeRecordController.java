package com.huohuzhihui.distribution.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.distribution.domain.DisUpgradeRecord;
import com.huohuzhihui.distribution.service.IDisUpgradeRecordService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 用户升级记录Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@RestController
@RequestMapping("/distribution/upgradeRecord")
public class DisUpgradeRecordController extends BaseController
{
    @Autowired
    private IDisUpgradeRecordService disUpgradeRecordService;

    /**
     * 查询用户升级记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:list')")
    @GetMapping("/list")
    public TableDataInfo list(DisUpgradeRecord disUpgradeRecord)
    {
        startPage();
        List<DisUpgradeRecord> list = disUpgradeRecordService.selectDisUpgradeRecordList(disUpgradeRecord);
        return getDataTable(list);
    }

    /**
     * 导出用户升级记录列表
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:export')")
    @Log(title = "用户升级记录", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DisUpgradeRecord disUpgradeRecord)
    {
        List<DisUpgradeRecord> list = disUpgradeRecordService.selectDisUpgradeRecordList(disUpgradeRecord);
        ExcelUtil<DisUpgradeRecord> util = new ExcelUtil<DisUpgradeRecord>(DisUpgradeRecord.class);
        return util.exportExcel(list, "upgradeRecord");
    }

    /**
     * 获取用户升级记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(disUpgradeRecordService.selectDisUpgradeRecordById(id));
    }

    /**
     * 新增用户升级记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:add')")
    @Log(title = "用户升级记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DisUpgradeRecord disUpgradeRecord)
    {
        return toAjax(disUpgradeRecordService.insertDisUpgradeRecord(disUpgradeRecord));
    }

    /**
     * 修改用户升级记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:edit')")
    @Log(title = "用户升级记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DisUpgradeRecord disUpgradeRecord)
    {
        return toAjax(disUpgradeRecordService.updateDisUpgradeRecord(disUpgradeRecord));
    }

    /**
     * 删除用户升级记录
     */
    @PreAuthorize("@ss.hasPermi('distribution:upgradeRecord:remove')")
    @Log(title = "用户升级记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(disUpgradeRecordService.deleteDisUpgradeRecordByIds(ids));
    }
}
