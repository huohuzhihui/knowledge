package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisRankParam;

/**
 * 分润参数设置Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisRankParamMapper 
{
    /**
     * 查询分润参数设置
     * 
     * @param id 分润参数设置ID
     * @return 分润参数设置
     */
    public DisRankParam selectDisRankParamById(Long id);

    /**
     * 查询分润参数设置列表
     * 
     * @param disRankParam 分润参数设置
     * @return 分润参数设置集合
     */
    public List<DisRankParam> selectDisRankParamList(DisRankParam disRankParam);

    /**
     * 新增分润参数设置
     * 
     * @param disRankParam 分润参数设置
     * @return 结果
     */
    public int insertDisRankParam(DisRankParam disRankParam);

    /**
     * 修改分润参数设置
     * 
     * @param disRankParam 分润参数设置
     * @return 结果
     */
    public int updateDisRankParam(DisRankParam disRankParam);

    /**
     * 删除分润参数设置
     * 
     * @param id 分润参数设置ID
     * @return 结果
     */
    public int deleteDisRankParamById(Long id);

    /**
     * 批量删除分润参数设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisRankParamByIds(Long[] ids);
}
