package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisProfitRecord;

/**
 * 分润记录Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisProfitRecordMapper 
{
    /**
     * 查询分润记录
     * 
     * @param id 分润记录ID
     * @return 分润记录
     */
    public DisProfitRecord selectDisProfitRecordById(Long id);

    /**
     * 查询分润记录列表
     * 
     * @param disProfitRecord 分润记录
     * @return 分润记录集合
     */
    public List<DisProfitRecord> selectDisProfitRecordList(DisProfitRecord disProfitRecord);

    /**
     * 新增分润记录
     * 
     * @param disProfitRecord 分润记录
     * @return 结果
     */
    public int insertDisProfitRecord(DisProfitRecord disProfitRecord);

    /**
     * 修改分润记录
     * 
     * @param disProfitRecord 分润记录
     * @return 结果
     */
    public int updateDisProfitRecord(DisProfitRecord disProfitRecord);

    /**
     * 删除分润记录
     * 
     * @param id 分润记录ID
     * @return 结果
     */
    public int deleteDisProfitRecordById(Long id);

    /**
     * 批量删除分润记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisProfitRecordByIds(Long[] ids);
}
