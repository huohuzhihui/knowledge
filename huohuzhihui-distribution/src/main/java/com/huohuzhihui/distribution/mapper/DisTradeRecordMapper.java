package com.huohuzhihui.distribution.mapper;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisTradeRecord;

/**
 * 交易金额记录Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface DisTradeRecordMapper 
{
    /**
     * 查询交易金额记录
     * 
     * @param id 交易金额记录ID
     * @return 交易金额记录
     */
    public DisTradeRecord selectDisTradeRecordById(Integer id);

    /**
     * 查询交易金额记录列表
     * 
     * @param disTradeRecord 交易金额记录
     * @return 交易金额记录集合
     */
    public List<DisTradeRecord> selectDisTradeRecordList(DisTradeRecord disTradeRecord);

    /**
     * 新增交易金额记录
     * 
     * @param disTradeRecord 交易金额记录
     * @return 结果
     */
    public int insertDisTradeRecord(DisTradeRecord disTradeRecord);

    /**
     * 修改交易金额记录
     * 
     * @param disTradeRecord 交易金额记录
     * @return 结果
     */
    public int updateDisTradeRecord(DisTradeRecord disTradeRecord);

    /**
     * 删除交易金额记录
     * 
     * @param id 交易金额记录ID
     * @return 结果
     */
    public int deleteDisTradeRecordById(Integer id);

    /**
     * 批量删除交易金额记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDisTradeRecordByIds(Integer[] ids);
}
