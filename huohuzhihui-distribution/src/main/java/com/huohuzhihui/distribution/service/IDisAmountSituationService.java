package com.huohuzhihui.distribution.service;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisAmountSituation;

/**
 * 账户变动，用于记录账户变动情况Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface IDisAmountSituationService 
{
    /**
     * 查询账户变动，用于记录账户变动情况
     * 
     * @param id 账户变动，用于记录账户变动情况ID
     * @return 账户变动，用于记录账户变动情况
     */
    public DisAmountSituation selectDisAmountSituationById(Long id);

    /**
     * 查询账户变动，用于记录账户变动情况列表
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 账户变动，用于记录账户变动情况集合
     */
    public List<DisAmountSituation> selectDisAmountSituationList(DisAmountSituation disAmountSituation);

    /**
     * 新增账户变动，用于记录账户变动情况
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 结果
     */
    public int insertDisAmountSituation(DisAmountSituation disAmountSituation);

    /**
     * 修改账户变动，用于记录账户变动情况
     * 
     * @param disAmountSituation 账户变动，用于记录账户变动情况
     * @return 结果
     */
    public int updateDisAmountSituation(DisAmountSituation disAmountSituation);

    /**
     * 批量删除账户变动，用于记录账户变动情况
     * 
     * @param ids 需要删除的账户变动，用于记录账户变动情况ID
     * @return 结果
     */
    public int deleteDisAmountSituationByIds(Long[] ids);

    /**
     * 删除账户变动，用于记录账户变动情况信息
     * 
     * @param id 账户变动，用于记录账户变动情况ID
     * @return 结果
     */
    public int deleteDisAmountSituationById(Long id);
}
