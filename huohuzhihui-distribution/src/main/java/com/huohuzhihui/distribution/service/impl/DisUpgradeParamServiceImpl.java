package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisUpgradeParamMapper;
import com.huohuzhihui.distribution.domain.DisUpgradeParam;
import com.huohuzhihui.distribution.service.IDisUpgradeParamService;

/**
 * 垂直升级配置Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisUpgradeParamServiceImpl implements IDisUpgradeParamService 
{
    @Autowired
    private DisUpgradeParamMapper disUpgradeParamMapper;

    /**
     * 查询垂直升级配置
     * 
     * @param id 垂直升级配置ID
     * @return 垂直升级配置
     */
    @Override
    public DisUpgradeParam selectDisUpgradeParamById(Long id)
    {
        return disUpgradeParamMapper.selectDisUpgradeParamById(id);
    }

    /**
     * 查询垂直升级配置列表
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 垂直升级配置
     */
    @Override
    public List<DisUpgradeParam> selectDisUpgradeParamList(DisUpgradeParam disUpgradeParam)
    {
        return disUpgradeParamMapper.selectDisUpgradeParamList(disUpgradeParam);
    }

    /**
     * 新增垂直升级配置
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 结果
     */
    @Override
    public int insertDisUpgradeParam(DisUpgradeParam disUpgradeParam)
    {
        return disUpgradeParamMapper.insertDisUpgradeParam(disUpgradeParam);
    }

    /**
     * 修改垂直升级配置
     * 
     * @param disUpgradeParam 垂直升级配置
     * @return 结果
     */
    @Override
    public int updateDisUpgradeParam(DisUpgradeParam disUpgradeParam)
    {
        disUpgradeParam.setUpdateTime(DateUtils.getNowDate());
        return disUpgradeParamMapper.updateDisUpgradeParam(disUpgradeParam);
    }

    /**
     * 批量删除垂直升级配置
     * 
     * @param ids 需要删除的垂直升级配置ID
     * @return 结果
     */
    @Override
    public int deleteDisUpgradeParamByIds(Long[] ids)
    {
        return disUpgradeParamMapper.deleteDisUpgradeParamByIds(ids);
    }

    /**
     * 删除垂直升级配置信息
     * 
     * @param id 垂直升级配置ID
     * @return 结果
     */
    @Override
    public int deleteDisUpgradeParamById(Long id)
    {
        return disUpgradeParamMapper.deleteDisUpgradeParamById(id);
    }
}
