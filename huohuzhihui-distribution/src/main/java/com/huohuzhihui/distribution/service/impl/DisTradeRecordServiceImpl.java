package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisTradeRecordMapper;
import com.huohuzhihui.distribution.domain.DisTradeRecord;
import com.huohuzhihui.distribution.service.IDisTradeRecordService;

/**
 * 交易金额记录Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisTradeRecordServiceImpl implements IDisTradeRecordService 
{
    @Autowired
    private DisTradeRecordMapper disTradeRecordMapper;

    /**
     * 查询交易金额记录
     * 
     * @param id 交易金额记录ID
     * @return 交易金额记录
     */
    @Override
    public DisTradeRecord selectDisTradeRecordById(Integer id)
    {
        return disTradeRecordMapper.selectDisTradeRecordById(id);
    }

    /**
     * 查询交易金额记录列表
     * 
     * @param disTradeRecord 交易金额记录
     * @return 交易金额记录
     */
    @Override
    public List<DisTradeRecord> selectDisTradeRecordList(DisTradeRecord disTradeRecord)
    {
        return disTradeRecordMapper.selectDisTradeRecordList(disTradeRecord);
    }

    /**
     * 新增交易金额记录
     * 
     * @param disTradeRecord 交易金额记录
     * @return 结果
     */
    @Override
    public int insertDisTradeRecord(DisTradeRecord disTradeRecord)
    {
        return disTradeRecordMapper.insertDisTradeRecord(disTradeRecord);
    }

    /**
     * 修改交易金额记录
     * 
     * @param disTradeRecord 交易金额记录
     * @return 结果
     */
    @Override
    public int updateDisTradeRecord(DisTradeRecord disTradeRecord)
    {
        return disTradeRecordMapper.updateDisTradeRecord(disTradeRecord);
    }

    /**
     * 批量删除交易金额记录
     * 
     * @param ids 需要删除的交易金额记录ID
     * @return 结果
     */
    @Override
    public int deleteDisTradeRecordByIds(Integer[] ids)
    {
        return disTradeRecordMapper.deleteDisTradeRecordByIds(ids);
    }

    /**
     * 删除交易金额记录信息
     * 
     * @param id 交易金额记录ID
     * @return 结果
     */
    @Override
    public int deleteDisTradeRecordById(Integer id)
    {
        return disTradeRecordMapper.deleteDisTradeRecordById(id);
    }
}
