package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisProfitRecordMapper;
import com.huohuzhihui.distribution.domain.DisProfitRecord;
import com.huohuzhihui.distribution.service.IDisProfitRecordService;

/**
 * 分润记录Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisProfitRecordServiceImpl implements IDisProfitRecordService 
{
    @Autowired
    private DisProfitRecordMapper disProfitRecordMapper;

    /**
     * 查询分润记录
     * 
     * @param id 分润记录ID
     * @return 分润记录
     */
    @Override
    public DisProfitRecord selectDisProfitRecordById(Long id)
    {
        return disProfitRecordMapper.selectDisProfitRecordById(id);
    }

    /**
     * 查询分润记录列表
     * 
     * @param disProfitRecord 分润记录
     * @return 分润记录
     */
    @Override
    public List<DisProfitRecord> selectDisProfitRecordList(DisProfitRecord disProfitRecord)
    {
        return disProfitRecordMapper.selectDisProfitRecordList(disProfitRecord);
    }

    /**
     * 新增分润记录
     * 
     * @param disProfitRecord 分润记录
     * @return 结果
     */
    @Override
    public int insertDisProfitRecord(DisProfitRecord disProfitRecord)
    {
        return disProfitRecordMapper.insertDisProfitRecord(disProfitRecord);
    }

    /**
     * 修改分润记录
     * 
     * @param disProfitRecord 分润记录
     * @return 结果
     */
    @Override
    public int updateDisProfitRecord(DisProfitRecord disProfitRecord)
    {
        disProfitRecord.setUpdateTime(DateUtils.getNowDate());
        return disProfitRecordMapper.updateDisProfitRecord(disProfitRecord);
    }

    /**
     * 批量删除分润记录
     * 
     * @param ids 需要删除的分润记录ID
     * @return 结果
     */
    @Override
    public int deleteDisProfitRecordByIds(Long[] ids)
    {
        return disProfitRecordMapper.deleteDisProfitRecordByIds(ids);
    }

    /**
     * 删除分润记录信息
     * 
     * @param id 分润记录ID
     * @return 结果
     */
    @Override
    public int deleteDisProfitRecordById(Long id)
    {
        return disProfitRecordMapper.deleteDisProfitRecordById(id);
    }
}
