package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisMemberAmountMapper;
import com.huohuzhihui.distribution.domain.DisMemberAmount;
import com.huohuzhihui.distribution.service.IDisMemberAmountService;

/**
 * 账户金额Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisMemberAmountServiceImpl implements IDisMemberAmountService 
{
    @Autowired
    private DisMemberAmountMapper disMemberAmountMapper;

    /**
     * 查询账户金额
     * 
     * @param id 账户金额ID
     * @return 账户金额
     */
    @Override
    public DisMemberAmount selectDisMemberAmountById(Long id)
    {
        return disMemberAmountMapper.selectDisMemberAmountById(id);
    }

    /**
     * 查询账户金额列表
     * 
     * @param disMemberAmount 账户金额
     * @return 账户金额
     */
    @Override
    public List<DisMemberAmount> selectDisMemberAmountList(DisMemberAmount disMemberAmount)
    {
        return disMemberAmountMapper.selectDisMemberAmountList(disMemberAmount);
    }

    /**
     * 新增账户金额
     * 
     * @param disMemberAmount 账户金额
     * @return 结果
     */
    @Override
    public int insertDisMemberAmount(DisMemberAmount disMemberAmount)
    {
        return disMemberAmountMapper.insertDisMemberAmount(disMemberAmount);
    }

    /**
     * 修改账户金额
     * 
     * @param disMemberAmount 账户金额
     * @return 结果
     */
    @Override
    public int updateDisMemberAmount(DisMemberAmount disMemberAmount)
    {
        disMemberAmount.setUpdateTime(DateUtils.getNowDate());
        return disMemberAmountMapper.updateDisMemberAmount(disMemberAmount);
    }

    /**
     * 批量删除账户金额
     * 
     * @param ids 需要删除的账户金额ID
     * @return 结果
     */
    @Override
    public int deleteDisMemberAmountByIds(Long[] ids)
    {
        return disMemberAmountMapper.deleteDisMemberAmountByIds(ids);
    }

    /**
     * 删除账户金额信息
     * 
     * @param id 账户金额ID
     * @return 结果
     */
    @Override
    public int deleteDisMemberAmountById(Long id)
    {
        return disMemberAmountMapper.deleteDisMemberAmountById(id);
    }
}
