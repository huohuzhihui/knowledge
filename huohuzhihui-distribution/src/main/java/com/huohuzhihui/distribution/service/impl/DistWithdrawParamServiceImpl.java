package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DistWithdrawParamMapper;
import com.huohuzhihui.distribution.domain.DistWithdrawParam;
import com.huohuzhihui.distribution.service.IDistWithdrawParamService;

/**
 * 提现收费配置Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DistWithdrawParamServiceImpl implements IDistWithdrawParamService 
{
    @Autowired
    private DistWithdrawParamMapper distWithdrawParamMapper;

    /**
     * 查询提现收费配置
     * 
     * @param id 提现收费配置ID
     * @return 提现收费配置
     */
    @Override
    public DistWithdrawParam selectDistWithdrawParamById(Long id)
    {
        return distWithdrawParamMapper.selectDistWithdrawParamById(id);
    }

    /**
     * 查询提现收费配置列表
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 提现收费配置
     */
    @Override
    public List<DistWithdrawParam> selectDistWithdrawParamList(DistWithdrawParam distWithdrawParam)
    {
        return distWithdrawParamMapper.selectDistWithdrawParamList(distWithdrawParam);
    }

    /**
     * 新增提现收费配置
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 结果
     */
    @Override
    public int insertDistWithdrawParam(DistWithdrawParam distWithdrawParam)
    {
        return distWithdrawParamMapper.insertDistWithdrawParam(distWithdrawParam);
    }

    /**
     * 修改提现收费配置
     * 
     * @param distWithdrawParam 提现收费配置
     * @return 结果
     */
    @Override
    public int updateDistWithdrawParam(DistWithdrawParam distWithdrawParam)
    {
        distWithdrawParam.setUpdateTime(DateUtils.getNowDate());
        return distWithdrawParamMapper.updateDistWithdrawParam(distWithdrawParam);
    }

    /**
     * 批量删除提现收费配置
     * 
     * @param ids 需要删除的提现收费配置ID
     * @return 结果
     */
    @Override
    public int deleteDistWithdrawParamByIds(Long[] ids)
    {
        return distWithdrawParamMapper.deleteDistWithdrawParamByIds(ids);
    }

    /**
     * 删除提现收费配置信息
     * 
     * @param id 提现收费配置ID
     * @return 结果
     */
    @Override
    public int deleteDistWithdrawParamById(Long id)
    {
        return distWithdrawParamMapper.deleteDistWithdrawParamById(id);
    }
}
