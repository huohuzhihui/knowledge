package com.huohuzhihui.distribution.service.impl;

import java.util.List;
import com.huohuzhihui.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.huohuzhihui.distribution.mapper.DisMemberInfoMapper;
import com.huohuzhihui.distribution.domain.DisMemberInfo;
import com.huohuzhihui.distribution.service.IDisMemberInfoService;

/**
 * 用户Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
@Service
public class DisMemberInfoServiceImpl implements IDisMemberInfoService 
{
    @Autowired
    private DisMemberInfoMapper disMemberInfoMapper;

    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    @Override
    public DisMemberInfo selectDisMemberInfoById(Long id)
    {
        return disMemberInfoMapper.selectDisMemberInfoById(id);
    }

    /**
     * 查询用户列表
     * 
     * @param disMemberInfo 用户
     * @return 用户
     */
    @Override
    public List<DisMemberInfo> selectDisMemberInfoList(DisMemberInfo disMemberInfo)
    {
        return disMemberInfoMapper.selectDisMemberInfoList(disMemberInfo);
    }

    /**
     * 新增用户
     * 
     * @param disMemberInfo 用户
     * @return 结果
     */
    @Override
    public int insertDisMemberInfo(DisMemberInfo disMemberInfo)
    {
        return disMemberInfoMapper.insertDisMemberInfo(disMemberInfo);
    }

    /**
     * 修改用户
     * 
     * @param disMemberInfo 用户
     * @return 结果
     */
    @Override
    public int updateDisMemberInfo(DisMemberInfo disMemberInfo)
    {
        disMemberInfo.setUpdateTime(DateUtils.getNowDate());
        return disMemberInfoMapper.updateDisMemberInfo(disMemberInfo);
    }

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    @Override
    public int deleteDisMemberInfoByIds(Long[] ids)
    {
        return disMemberInfoMapper.deleteDisMemberInfoByIds(ids);
    }

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    @Override
    public int deleteDisMemberInfoById(Long id)
    {
        return disMemberInfoMapper.deleteDisMemberInfoById(id);
    }
}
