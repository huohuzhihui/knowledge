package com.huohuzhihui.distribution.service;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisMemberInfo;

/**
 * 用户Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface IDisMemberInfoService 
{
    /**
     * 查询用户
     * 
     * @param id 用户ID
     * @return 用户
     */
    public DisMemberInfo selectDisMemberInfoById(Long id);

    /**
     * 查询用户列表
     * 
     * @param disMemberInfo 用户
     * @return 用户集合
     */
    public List<DisMemberInfo> selectDisMemberInfoList(DisMemberInfo disMemberInfo);

    /**
     * 新增用户
     * 
     * @param disMemberInfo 用户
     * @return 结果
     */
    public int insertDisMemberInfo(DisMemberInfo disMemberInfo);

    /**
     * 修改用户
     * 
     * @param disMemberInfo 用户
     * @return 结果
     */
    public int updateDisMemberInfo(DisMemberInfo disMemberInfo);

    /**
     * 批量删除用户
     * 
     * @param ids 需要删除的用户ID
     * @return 结果
     */
    public int deleteDisMemberInfoByIds(Long[] ids);

    /**
     * 删除用户信息
     * 
     * @param id 用户ID
     * @return 结果
     */
    public int deleteDisMemberInfoById(Long id);
}
