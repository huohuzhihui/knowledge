package com.huohuzhihui.distribution.service;

import java.util.List;
import com.huohuzhihui.distribution.domain.DisWithdrawRecord;

/**
 * 提现Service接口
 * 
 * @author huohuzhihui
 * @date 2020-12-27
 */
public interface IDisWithdrawRecordService 
{
    /**
     * 查询提现
     * 
     * @param id 提现ID
     * @return 提现
     */
    public DisWithdrawRecord selectDisWithdrawRecordById(Long id);

    /**
     * 查询提现列表
     * 
     * @param disWithdrawRecord 提现
     * @return 提现集合
     */
    public List<DisWithdrawRecord> selectDisWithdrawRecordList(DisWithdrawRecord disWithdrawRecord);

    /**
     * 新增提现
     * 
     * @param disWithdrawRecord 提现
     * @return 结果
     */
    public int insertDisWithdrawRecord(DisWithdrawRecord disWithdrawRecord);

    /**
     * 修改提现
     * 
     * @param disWithdrawRecord 提现
     * @return 结果
     */
    public int updateDisWithdrawRecord(DisWithdrawRecord disWithdrawRecord);

    /**
     * 批量删除提现
     * 
     * @param ids 需要删除的提现ID
     * @return 结果
     */
    public int deleteDisWithdrawRecordByIds(Long[] ids);

    /**
     * 删除提现信息
     * 
     * @param id 提现ID
     * @return 结果
     */
    public int deleteDisWithdrawRecordById(Long id);
}
