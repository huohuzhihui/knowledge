import request from '@/utils/request'

// 查询分润参数设置列表
export function listRankParam(query) {
  return request({
    url: '/distribution/rankParam/list',
    method: 'get',
    params: query
  })
}

// 查询分润参数设置详细
export function getRankParam(id) {
  return request({
    url: '/distribution/rankParam/' + id,
    method: 'get'
  })
}

// 新增分润参数设置
export function addRankParam(data) {
  return request({
    url: '/distribution/rankParam',
    method: 'post',
    data: data
  })
}

// 修改分润参数设置
export function updateRankParam(data) {
  return request({
    url: '/distribution/rankParam',
    method: 'put',
    data: data
  })
}

// 删除分润参数设置
export function delRankParam(id) {
  return request({
    url: '/distribution/rankParam/' + id,
    method: 'delete'
  })
}

// 导出分润参数设置
export function exportRankParam(query) {
  return request({
    url: '/distribution/rankParam/export',
    method: 'get',
    params: query
  })
}