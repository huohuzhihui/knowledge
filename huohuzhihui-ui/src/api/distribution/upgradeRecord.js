import request from '@/utils/request'

// 查询用户升级记录列表
export function listUpgradeRecord(query) {
  return request({
    url: '/distribution/upgradeRecord/list',
    method: 'get',
    params: query
  })
}

// 查询用户升级记录详细
export function getUpgradeRecord(id) {
  return request({
    url: '/distribution/upgradeRecord/' + id,
    method: 'get'
  })
}

// 新增用户升级记录
export function addUpgradeRecord(data) {
  return request({
    url: '/distribution/upgradeRecord',
    method: 'post',
    data: data
  })
}

// 修改用户升级记录
export function updateUpgradeRecord(data) {
  return request({
    url: '/distribution/upgradeRecord',
    method: 'put',
    data: data
  })
}

// 删除用户升级记录
export function delUpgradeRecord(id) {
  return request({
    url: '/distribution/upgradeRecord/' + id,
    method: 'delete'
  })
}

// 导出用户升级记录
export function exportUpgradeRecord(query) {
  return request({
    url: '/distribution/upgradeRecord/export',
    method: 'get',
    params: query
  })
}