import request from '@/utils/request'

// 查询分润参数设置列表
export function listParam(query) {
  return request({
    url: '/distribution/profit/param/list',
    method: 'get',
    params: query
  })
}

// 查询分润参数设置详细
export function getParam(id) {
  return request({
    url: '/distribution/profit/param/' + id,
    method: 'get'
  })
}

// 新增分润参数设置
export function addParam(data) {
  return request({
    url: '/distribution/profit/param',
    method: 'post',
    data: data
  })
}

// 修改分润参数设置
export function updateParam(data) {
  return request({
    url: '/distribution/profit/param',
    method: 'put',
    data: data
  })
}

// 删除分润参数设置
export function delParam(id) {
  return request({
    url: '/distribution/profit/param/' + id,
    method: 'delete'
  })
}

// 导出分润参数设置
export function exportParam(query) {
  return request({
    url: '/distribution/profit/param/export',
    method: 'get',
    params: query
  })
}
