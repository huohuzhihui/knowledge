import request from '@/utils/request'

// 查询升级配置列表
export function listUpgradeParam(query) {
  return request({
    url: '/distribution/upgradeParam/list',
    method: 'get',
    params: query
  })
}

// 查询升级配置详细
export function getUpgradeParam(id) {
  return request({
    url: '/distribution/upgradeParam/' + id,
    method: 'get'
  })
}

// 新增升级配置
export function addUpgradeParam(data) {
  return request({
    url: '/distribution/upgradeParam',
    method: 'post',
    data: data
  })
}

// 修改升级配置
export function updateUpgradeParam(data) {
  return request({
    url: '/distribution/upgradeParam',
    method: 'put',
    data: data
  })
}

// 删除升级配置
export function delUpgradeParam(id) {
  return request({
    url: '/distribution/upgradeParam/' + id,
    method: 'delete'
  })
}

// 导出升级配置
export function exportUpgradeParam(query) {
  return request({
    url: '/distribution/upgradeParam/export',
    method: 'get',
    params: query
  })
}