import request from '@/utils/request'

// 查询微信模板列表
export function listWeixinTemplate(query) {
  return request({
    url: '/weixin/weixinTemplate/list',
    method: 'get',
    params: query
  })
}

// 查询微信模板详细
export function getWeixinTemplate(id) {
  return request({
    url: '/weixin/weixinTemplate/' + id,
    method: 'get'
  })
}

// 新增微信模板
export function addWeixinTemplate(data) {
  return request({
    url: '/weixin/weixinTemplate',
    method: 'post',
    data: data
  })
}

// 修改微信模板
export function updateWeixinTemplate(data) {
  return request({
    url: '/weixin/weixinTemplate',
    method: 'put',
    data: data
  })
}

// 删除微信模板
export function delWeixinTemplate(id) {
  return request({
    url: '/weixin/weixinTemplate/' + id,
    method: 'delete'
  })
}

// 导出微信模板
export function exportWeixinTemplate(query) {
  return request({
    url: '/weixin/weixinTemplate/export',
    method: 'get',
    params: query
  })
}