import request from '@/utils/request'

// 查询微信公众号设置列表
export function listWeixinSetting(query) {
  return request({
    url: '/weixin/weixinSetting/list',
    method: 'get',
    params: query
  })
}

// 查询微信公众号设置详细
export function getWeixinSetting(id) {
  return request({
    url: '/weixin/weixinSetting/' + id,
    method: 'get'
  })
}

// 新增微信公众号设置
export function addWeixinSetting(data) {
  return request({
    url: '/weixin/weixinSetting',
    method: 'post',
    data: data
  })
}

// 修改微信公众号设置
export function updateWeixinSetting(data) {
  return request({
    url: '/weixin/weixinSetting',
    method: 'put',
    data: data
  })
}

// 删除微信公众号设置
export function delWeixinSetting(id) {
  return request({
    url: '/weixin/weixinSetting/' + id,
    method: 'delete'
  })
}

// 导出微信公众号设置
export function exportWeixinSetting(query) {
  return request({
    url: '/weixin/weixinSetting/export',
    method: 'get',
    params: query
  })
}