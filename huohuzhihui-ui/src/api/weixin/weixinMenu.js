import request from '@/utils/request'

// 查询微信公众号菜单列表
export function listWeixinMenu(query) {
  return request({
    url: '/weixin/weixinMenu/list',
    method: 'get',
    params: query
  })
}

// 查询微信公众号菜单详细
export function getWeixinMenu(menuId) {
  return request({
    url: '/weixin/weixinMenu/' + menuId,
    method: 'get'
  })
}

// 新增微信公众号菜单
export function addWeixinMenu(data) {
  return request({
    url: '/weixin/weixinMenu',
    method: 'post',
    data: data
  })
}

// 修改微信公众号菜单
export function updateWeixinMenu(data) {
  return request({
    url: '/weixin/weixinMenu',
    method: 'put',
    data: data
  })
}

// 删除微信公众号菜单
export function delWeixinMenu(menuId) {
  return request({
    url: '/weixin/weixinMenu/' + menuId,
    method: 'delete'
  })
}

// 导出微信公众号菜单
export function exportWeixinMenu(query) {
  return request({
    url: '/weixin/weixinMenu/export',
    method: 'get',
    params: query
  })
}