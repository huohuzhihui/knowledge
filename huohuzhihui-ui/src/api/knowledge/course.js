import request from '@/utils/request'

// 查询知识/课程列表
export function listCourse(query) {
  return request({
    url: '/knowledge/course/list',
    method: 'get',
    params: query
  })
}

// 查询知识/课程详细
export function getCourse(courseId) {
  return request({
    url: '/knowledge/course/' + courseId,
    method: 'get'
  })
}

// 新增知识/课程
export function addCourse(data) {
  return request({
    url: '/knowledge/course',
    method: 'post',
    data: data
  })
}

// 修改知识/课程
export function updateCourse(data) {
  return request({
    url: '/knowledge/course',
    method: 'put',
    data: data
  })
}

// 删除知识/课程
export function delCourse(courseId) {
  return request({
    url: '/knowledge/course/' + courseId,
    method: 'delete'
  })
}

// 导出知识/课程
export function exportCourse(query) {
  return request({
    url: '/knowledge/course/export',
    method: 'get',
    params: query
  })
}

// 查询知识分类
export function getAllCategory() {
  return request({
    url: '/knowledge/category/getAllCategory',
    method: 'get'
  })
}

// 上传
export function uploadAction() {
  return request({
    url: '/common/upload',
    method: 'post'
  })
}
