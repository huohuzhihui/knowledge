package com.huohuzhihui.web.controller.knowledge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.huohuzhihui.common.core.domain.TreeSelect;
import com.huohuzhihui.common.core.domain.entity.SysMenu;
import com.huohuzhihui.common.core.domain.model.LoginUser;
import com.huohuzhihui.common.utils.ServletUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.service.IKnowledgeCategoryService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 知识分类Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@RestController("category")
@RequestMapping("/knowledge/category")
public class KnowledgeCategoryController extends BaseController
{
    @Autowired
    private IKnowledgeCategoryService knowledgeCategoryService;

    /**
     * 查询知识分类列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(KnowledgeCategory knowledgeCategory)
    {
        startPage();
        List<KnowledgeCategory> list = knowledgeCategoryService.selectKnowledgeCategoryList(knowledgeCategory);
        return getDataTable(list);
    }

    /**
     * 导出知识分类列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:export')")
    @Log(title = "知识分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(KnowledgeCategory knowledgeCategory)
    {
        List<KnowledgeCategory> list = knowledgeCategoryService.selectKnowledgeCategoryList(knowledgeCategory);
        ExcelUtil<KnowledgeCategory> util = new ExcelUtil<KnowledgeCategory>(KnowledgeCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取知识分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:query')")
    @GetMapping(value = "/{categoryId}")
    public AjaxResult getInfo(@PathVariable("categoryId") Long categoryId)
    {
        return AjaxResult.success(knowledgeCategoryService.selectKnowledgeCategoryById(categoryId));
    }

    /**
     * 新增知识分类
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:add')")
    @Log(title = "知识分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody KnowledgeCategory knowledgeCategory)
    {
        return toAjax(knowledgeCategoryService.insertKnowledgeCategory(knowledgeCategory));
    }

    /**
     * 修改知识分类
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:edit')")
    @Log(title = "知识分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody KnowledgeCategory knowledgeCategory)
    {
        return toAjax(knowledgeCategoryService.updateKnowledgeCategory(knowledgeCategory));
    }

    /**
     * 删除知识分类
     */
    @PreAuthorize("@ss.hasPermi('knowledge:category:remove')")
    @Log(title = "知识分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{categoryIds}")
    public AjaxResult remove(@PathVariable Long[] categoryIds)
    {
        return toAjax(knowledgeCategoryService.deleteKnowledgeCategoryByIds(categoryIds));
    }

    /**
     * 获取所有分类
     * @return
     */
    @GetMapping(value = "/getAllCategory")
    public List<KnowledgeCategory> getAllCategory()
    {
        KnowledgeCategory category = new KnowledgeCategory();
        List<KnowledgeCategory> list = knowledgeCategoryService.selectKnowledgeCategoryList(category);
        return list;
    }
}
