package com.huohuzhihui.web.controller.api;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.alibaba.fastjson.JSON;
import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.account.service.IAccUserAccountService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.domain.model.LoginUser;
import com.huohuzhihui.common.utils.SmsVerificationCodeService;
import com.huohuzhihui.framework.web.service.TokenService;
import com.huohuzhihui.wxminiapp.domain.WxLoginRequest;
import com.huohuzhihui.wxminiapp.service.WxService;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *微信api
 * 
 * @author lizhihe
 * @date 2020-11-22
 */
@RestController
@RequestMapping("/api/sms")
public class ApiSmsController extends BaseController
{
    @Autowired
    private SmsVerificationCodeService smsVerificationCodeService;


    @PostMapping("/sendCode")
    public AjaxResult sendCode(String mobile) {
        SendSmsResponse response = smsVerificationCodeService.send(mobile);
        return new AjaxResult(200, "发送验证码", JSON.toJSON(response));
    }

}
