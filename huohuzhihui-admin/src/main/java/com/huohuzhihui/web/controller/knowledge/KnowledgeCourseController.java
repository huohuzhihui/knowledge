package com.huohuzhihui.web.controller.knowledge;

import java.util.List;

import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.service.IKnowleageCourseService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 知识/课程Controller
 * 
 * @author huohuzhihui
 * @date 2020-12-12
 */
@RestController
@RequestMapping("/knowledge/course")
public class KnowledgeCourseController extends BaseController
{
    @Autowired
    private IKnowleageCourseService knowleageCourseService;

    /**
     * 查询知识/课程列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:list')")
    @GetMapping("/list")
    public TableDataInfo list(KnowleageCourse knowleageCourse)
    {
        startPage();
        List<KnowleageCourse> list = knowleageCourseService.selectKnowleageCourseList(knowleageCourse);
        return getDataTable(list);
    }

    /**
     * 导出知识/课程列表
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:export')")
    @Log(title = "知识/课程", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(KnowleageCourse knowleageCourse)
    {
        List<KnowleageCourse> list = knowleageCourseService.selectKnowleageCourseList(knowleageCourse);
        ExcelUtil<KnowleageCourse> util = new ExcelUtil<KnowleageCourse>(KnowleageCourse.class);
        return util.exportExcel(list, "course");
    }

    /**
     * 获取知识/课程详细信息
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:query')")
    @GetMapping(value = "/{courseId}")
    public AjaxResult getInfo(@PathVariable("courseId") Long courseId)
    {
        return AjaxResult.success(knowleageCourseService.selectKnowleageCourseById(courseId));
    }

    /**
     * 新增知识/课程
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:add')")
    @Log(title = "知识/课程", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody KnowleageCourse knowleageCourse)
    {
        return toAjax(knowleageCourseService.insertKnowleageCourse(knowleageCourse));
    }

    /**
     * 修改知识/课程
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:edit')")
    @Log(title = "知识/课程", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody KnowleageCourse knowleageCourse)
    {
        return toAjax(knowleageCourseService.updateKnowleageCourse(knowleageCourse));
    }

    /**
     * 删除知识/课程
     */
    @PreAuthorize("@ss.hasPermi('knowledge:course:remove')")
    @Log(title = "知识/课程", businessType = BusinessType.DELETE)
	@DeleteMapping("/{courseIds}")
    public AjaxResult remove(@PathVariable Long[] courseIds)
    {
        return toAjax(knowleageCourseService.deleteKnowleageCourseByIds(courseIds));
    }

    /**
     * 获取所有课程
     * @return
     */
    @GetMapping(value = "/getAllCourse")
    public List<KnowleageCourse> getAllCourse()
    {
        KnowleageCourse course = new KnowleageCourse();
        List<KnowleageCourse> list = knowleageCourseService.selectKnowleageCourseList(course);
        return list;
    }
}
