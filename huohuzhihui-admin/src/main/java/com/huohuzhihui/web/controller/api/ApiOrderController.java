package com.huohuzhihui.web.controller.api;

import com.huohuzhihui.api.domain.ApiOrder;
import com.huohuzhihui.api.service.ApiAccountService;
import com.huohuzhihui.api.service.ApiOrderService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.domain.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/api/order")
public class ApiOrderController extends BaseController
{
    @Autowired
    private ApiOrderService apiOrderService;


    @PostMapping("/findOrderListByUserId")
    @ResponseBody
    public AjaxResult findOrderListByUserId(@RequestBody ApiOrder apiOrder){
        return AjaxResult.success(this.apiOrderService.findOrderListByUserId(apiOrder.getUserId()));
    }

}
