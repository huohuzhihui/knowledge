package com.huohuzhihui.web.controller.api;

import com.huohuzhihui.api.service.ApiKnowledgeService;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户Controller
 * 
 * @author zylu
 * @date 2020-11-14
 */
@RestController
@RequestMapping("/api/knowledge")
public class ApiKnowledgeController extends BaseController
{
    @Autowired
    private ApiKnowledgeService apiKnowledgeService;



    @PostMapping("/findKnowledgeCategoryList")
    @ResponseBody
    public AjaxResult findKnowledgeCategoryList(@RequestBody  KnowledgeCategory knowledgeCategory){
        List<KnowledgeCategory> list = apiKnowledgeService.findKnowledgeCategoryList(knowledgeCategory);
        return AjaxResult.success(list);
    }



    @PostMapping("/findKnowledgeCourseList")
    @ResponseBody
    public AjaxResult findKnowledgeCourseList(@RequestBody KnowleageCourse knowleageCourse){
        List<KnowleageCourse> list = apiKnowledgeService.findKnowledgeCourseList(knowleageCourse);
        return AjaxResult.success(list);
    }


    @PostMapping("/getCourseById")
    @ResponseBody
    public AjaxResult getCourseById(@RequestBody KnowleageCourse knowleageCourse){
        return AjaxResult.success(apiKnowledgeService.getCourseById(knowleageCourse));
    }
    @PostMapping("/findChapterListByCourseId")
    @ResponseBody
    public AjaxResult findChapterListByCourseId(@RequestBody KnowledgeChapter knowledgeChapter){
        List<KnowledgeChapter> list = apiKnowledgeService.findChapterListByCourseId(knowledgeChapter);
        return AjaxResult.success(list);
    }

    @PostMapping("/getChapterByChapterId")
    @ResponseBody
    public AjaxResult getChapterByChapterId(@RequestBody KnowledgeChapter knowledgeChapter){
        KnowledgeChapter result = apiKnowledgeService.getChapterByChapterId(knowledgeChapter.getChapterId());
        return AjaxResult.success(result);
    }

}
