package com.huohuzhihui.weixin.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.huohuzhihui.common.annotation.Log;
import com.huohuzhihui.common.core.controller.BaseController;
import com.huohuzhihui.common.core.domain.AjaxResult;
import com.huohuzhihui.common.enums.BusinessType;
import com.huohuzhihui.weixin.domain.WeixinMenu;
import com.huohuzhihui.weixin.service.IWeixinMenuService;
import com.huohuzhihui.common.utils.poi.ExcelUtil;
import com.huohuzhihui.common.core.page.TableDataInfo;

/**
 * 微信公众号菜单Controller
 * 
 * @author huohuzhihui
 * @date 2021-01-01
 */
@RestController
@RequestMapping("/weixin/weixinMenu")
public class WeixinMenuController extends BaseController
{
    @Autowired
    private IWeixinMenuService weixinMenuService;

    /**
     * 查询微信公众号菜单列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:list')")
    @GetMapping("/list")
    public TableDataInfo list(WeixinMenu weixinMenu)
    {
        startPage();
        List<WeixinMenu> list = weixinMenuService.selectMenuList(weixinMenu);
        return getDataTable(list);
    }

    /**
     * 导出微信公众号菜单列表
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:export')")
    @Log(title = "微信公众号菜单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WeixinMenu weixinMenu)
    {
        List<WeixinMenu> list = weixinMenuService.selectMenuList(weixinMenu);
        ExcelUtil<WeixinMenu> util = new ExcelUtil<WeixinMenu>(WeixinMenu.class);
        return util.exportExcel(list, "weixinMenu");
    }

    /**
     * 获取微信公众号菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:query')")
    @GetMapping(value = "/{menuId}")
    public AjaxResult getInfo(@PathVariable("menuId") Long menuId)
    {
        return AjaxResult.success(weixinMenuService.selectMenuById(menuId));
    }

    /**
     * 新增微信公众号菜单
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:add')")
    @Log(title = "微信公众号菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WeixinMenu weixinMenu)
    {
        return AjaxResult.success();//toAjax(weixinMenuService.insertWeixinMenu(weixinMenu));
    }

    /**
     * 修改微信公众号菜单
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:edit')")
    @Log(title = "微信公众号菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WeixinMenu weixinMenu)
    {
        return AjaxResult.success();//toAjax(weixinMenuService.updateWeixinMenu(weixinMenu));
    }

    /**
     * 删除微信公众号菜单
     */
    @PreAuthorize("@ss.hasPermi('weixin:weixinMenu:remove')")
    @Log(title = "微信公众号菜单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuIds}")
    public AjaxResult remove(@PathVariable Long[] menuIds)
    {
        return AjaxResult.success();//toAjax(weixinMenuService.deleteWeixinMenuByIds(menuIds));
    }
}
