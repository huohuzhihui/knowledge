package com.huohuzhihui.trade.mapper;

import java.util.List;
import com.huohuzhihui.trade.domain.TradeOrderDetail;

/**
 * 订单明细Mapper接口
 * 
 * @author huohuzhihui
 * @date 2020-12-14
 */
public interface TradeOrderDetailMapper 
{
    /**
     * 查询订单明细
     * 
     * @param id 订单明细ID
     * @return 订单明细
     */
    public TradeOrderDetail selectTradeOrderDetailById(Long id);

    /**
     * 查询订单明细列表
     * 
     * @param tradeOrderDetail 订单明细
     * @return 订单明细集合
     */
    public List<TradeOrderDetail> selectTradeOrderDetailList(TradeOrderDetail tradeOrderDetail);

    /**
     * 新增订单明细
     * 
     * @param tradeOrderDetail 订单明细
     * @return 结果
     */
    public int insertTradeOrderDetail(TradeOrderDetail tradeOrderDetail);

    /**
     * 修改订单明细
     * 
     * @param tradeOrderDetail 订单明细
     * @return 结果
     */
    public int updateTradeOrderDetail(TradeOrderDetail tradeOrderDetail);

    /**
     * 删除订单明细
     * 
     * @param id 订单明细ID
     * @return 结果
     */
    public int deleteTradeOrderDetailById(Long id);

    /**
     * 批量删除订单明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTradeOrderDetailByIds(Long[] ids);
}
