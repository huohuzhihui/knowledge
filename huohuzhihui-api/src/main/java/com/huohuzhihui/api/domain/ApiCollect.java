package com.huohuzhihui.api.domain;

import com.huohuzhihui.common.annotation.Excel;
import com.huohuzhihui.common.core.domain.BaseEntity;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 收藏对象 acc_collect
 * 
 * @author huohuzhihui
 * @date 2020-12-20
 */
public class ApiCollect extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户id */
    @Excel(name = "用户id")
    private Long userId;

    /** 收藏对应的商品或者物品id */
    @Excel(name = "收藏对应的商品或者物品id")
    private Long sourceId;

    /** 收藏类型 1商品 2课程 */
    @Excel(name = "收藏类型 1商品 2课程")
    private Integer type;

    /** 状态 1有效 0无效 */
    @Excel(name = "状态 1有效 0无效")
    private Integer status;


    private KnowleageCourse knowleageCourse;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setSourceId(Long sourceId) 
    {
        this.sourceId = sourceId;
    }

    public Long getSourceId() 
    {
        return sourceId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public KnowleageCourse getKnowleageCourse() {
        return knowleageCourse;
    }

    public void setKnowleageCourse(KnowleageCourse knowleageCourse) {
        this.knowleageCourse = knowleageCourse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("sourceId", getSourceId())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .toString();
    }
}
