package com.huohuzhihui.api.service;

import com.huohuzhihui.account.domain.AccCollect;
import com.huohuzhihui.api.domain.ApiCollect;

import java.util.List;

public interface ApiCollectService {
    public int addCollect(ApiCollect apiCollect);

    public List<ApiCollect> findCollectList(ApiCollect apiCollect);

    public AccCollect findCollectByUserIdAndSourceId(Long userId, Long sourceId);

    public int deleteCollect(Long id);
}
