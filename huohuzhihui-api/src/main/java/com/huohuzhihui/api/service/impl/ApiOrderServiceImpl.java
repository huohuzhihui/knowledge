package com.huohuzhihui.api.service.impl;

import com.alibaba.fastjson.JSON;
import com.huohuzhihui.api.domain.ApiOrder;
import com.huohuzhihui.api.service.ApiOrderService;
import com.huohuzhihui.api.service.ApiPayService;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.exception.CustomException;
import com.huohuzhihui.common.utils.uuid.IdUtils;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.mapper.KnowleageCourseMapper;
import com.huohuzhihui.system.service.ISysConfigService;
import com.huohuzhihui.trade.domain.TradeOrder;
import com.huohuzhihui.trade.domain.TradeOrderDetail;
import com.huohuzhihui.trade.mapper.AccTradeRecordMapper;
import com.huohuzhihui.trade.mapper.TradeOrderDetailMapper;
import com.huohuzhihui.trade.mapper.TradeOrderMapper;
import com.huohuzhihui.trade.service.ITradeOrderService;
import com.ijpay.core.enums.SignType;
import com.ijpay.core.enums.TradeType;
import com.ijpay.core.kit.WxPayKit;
import com.ijpay.wxpay.WxPayApi;
import com.ijpay.wxpay.WxPayApiConfig;
import com.ijpay.wxpay.model.UnifiedOrderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 订单Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
@Service
public class ApiOrderServiceImpl implements ApiOrderService {
    @Autowired
    private TradeOrderMapper tradeOrderMapper;
    @Autowired
    private TradeOrderDetailMapper tradeOrderDetailMapper;

    @Override
    public List<TradeOrderDetail> findOrderListByUserId(Long userId) {
        TradeOrder tradeOrder = new TradeOrder();
        tradeOrder.setUserId(userId);
        tradeOrder.setStatus(1);//支付成功的
        List<TradeOrder> tradeOrderList = this.tradeOrderMapper.selectTradeOrderList(tradeOrder);
        if(tradeOrderList!=null && tradeOrderList.size()>0 ){
            List<TradeOrderDetail> list = new ArrayList<TradeOrderDetail>();

            for(int i = 0 ; i < tradeOrderList.size(); i++){
                TradeOrder order = tradeOrderList.get(i);

                TradeOrderDetail tradeOrderDetail = new TradeOrderDetail();
                tradeOrderDetail.setOrderId(order.getId());

                List<TradeOrderDetail> tradeOrderDetailList = this.tradeOrderDetailMapper.selectTradeOrderDetailList(tradeOrderDetail);
                list.addAll(tradeOrderDetailList);
            }
            return list;
        }
        return null;
    }
}

