package com.huohuzhihui.api.service;

import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import com.huohuzhihui.system.domain.SysNotice;

import java.util.List;

/**
 * 通知公告Service接口
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
public interface ApiKnowledgeService
{
  public List<KnowledgeCategory> findKnowledgeCategoryList(KnowledgeCategory knowledgeCategory);

  public List<KnowleageCourse> findKnowledgeCourseList(KnowleageCourse knowleageCourse);

  public  KnowleageCourse getCourseById(KnowleageCourse knowleageCourse);

  public List<KnowledgeChapter> findChapterListByCourseId(KnowledgeChapter knowledgeChapter);

  public KnowledgeChapter getChapterByChapterId(Long chapterId);
}
