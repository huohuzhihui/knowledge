package com.huohuzhihui.api.service;

import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.system.domain.SysNotice;

import java.math.BigDecimal;
import java.util.List;

/**
 * 通知公告Service接口
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
public interface ApiPayService
{

    String addOrder(SysUser sysUser , BigDecimal amount, String source, String channelCode, String openId, String ip,Long courseId);

    String webPay(Long userId, String amount, String source, String channelCode, String openId, String ip, Long courseId);

    String queryOrder(String outTradeNo);
}
