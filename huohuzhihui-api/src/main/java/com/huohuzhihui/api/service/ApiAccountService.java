package com.huohuzhihui.api.service;

import com.huohuzhihui.account.domain.AccCard;
import com.huohuzhihui.account.domain.AccUserAccount;
import com.huohuzhihui.api.domain.ApiUser;
import com.huohuzhihui.common.core.domain.entity.SysUser;
import com.huohuzhihui.common.core.domain.model.LoginUser;
import com.huohuzhihui.trade.domain.TradeOrder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

/**
 * 订单Service接口
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
public interface ApiAccountService
{

    public AccUserAccount getAccountByPhonenumber(String phonenumber);

    public AccUserAccount getAccountById(Long accountId);

    public AccUserAccount getAccountByUserId(Long userId);

    public SysUser getUserByPhonenumber(String phonenumber);

    public SysUser getUserById(Long userId);

    public int  resetPassword(String userName, String newPassword);

    public BigDecimal getSumTrade(TradeOrder tradeOrder);

    public List<TradeOrder> findAccountTradeRecordList(TradeOrder tradeOrder);

    public String recharge(AccUserAccount accUserAccount, BigDecimal amount, String source, String channelCode,String openId,String ip);

    public String wxPayNotify(HttpServletRequest request);

    public LoginUser login(String phonenumber, String password);

    public LoginUser freshUser(LoginUser user);

    public SysUser updateUser(SysUser user);
}
