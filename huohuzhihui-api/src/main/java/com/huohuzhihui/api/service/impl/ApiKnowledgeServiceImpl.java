package com.huohuzhihui.api.service.impl;

import com.huohuzhihui.api.service.ApiKnowledgeService;
import com.huohuzhihui.knowledge.domain.KnowleageCourse;
import com.huohuzhihui.knowledge.domain.KnowledgeCategory;
import com.huohuzhihui.knowledge.domain.KnowledgeChapter;
import com.huohuzhihui.knowledge.mapper.KnowleageCourseMapper;
import com.huohuzhihui.knowledge.mapper.KnowledgeCategoryMapper;
import com.huohuzhihui.knowledge.mapper.KnowledgeChapterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单Service业务层处理
 * 
 * @author huohuzhihui
 * @date 2020-11-15
 */
@Service
public class ApiKnowledgeServiceImpl implements ApiKnowledgeService {
    @Autowired
    private KnowledgeCategoryMapper knowledgeCategoryMapper;
    @Autowired
    private KnowleageCourseMapper knowleageCourseMapper;
    @Autowired
    private KnowledgeChapterMapper knowledgeChapterMapper;

    @Override
    public List<KnowledgeCategory> findKnowledgeCategoryList(KnowledgeCategory knowledgeCategory) {
        return this.knowledgeCategoryMapper.selectKnowledgeCategoryList(knowledgeCategory);
    }

    @Override
    public List<KnowleageCourse> findKnowledgeCourseList(KnowleageCourse knowleageCourse) {
        return knowleageCourseMapper.selectKnowleageCourseList(knowleageCourse);
    }

    @Override
    public KnowleageCourse getCourseById(KnowleageCourse knowleageCourse) {
        return knowleageCourseMapper.selectKnowleageCourseById(knowleageCourse.getCourseId());
    }

    @Override
    public List<KnowledgeChapter> findChapterListByCourseId(KnowledgeChapter knowledgeChapter) {
        return knowledgeChapterMapper.selectKnowledgeChapterList(knowledgeChapter);
    }

    @Override
    public KnowledgeChapter getChapterByChapterId(Long chapterId) {
        return knowledgeChapterMapper.selectKnowledgeChapterById(chapterId);
    }
}

